MAPS
====

Run configuration
-----------------
Boot class: jade.Boot
VM Options: -Djava.util.logging.config.file=YOURPROJECTPATH\jadeLogging.properties
Program arguments: -gui -services jade.core.event.NotificationService;jade.core.messaging.TopicManagementService MyCreator:it.rse.retiattive.acs.agent.Creator(platforms/PlanControlPlatform.xml)
