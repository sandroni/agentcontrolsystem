app.factory('AgentList', ['$http', function($http) {
  return function() {
    return $http.get('/api/agents')
            .success(function(data) {
              return data;
            })
            .error(function(err) {
              return err;
            });
    }
}]);