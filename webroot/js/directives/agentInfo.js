app.directive('agentInfo', function() {
  return {
    restrict: 'E',
    scope: {
      details: '='
    },
    templateUrl: 'js/directives/agentInfo.html'
  };
});