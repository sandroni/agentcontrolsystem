app.controller('AgentsController', ['$scope', '$interval', 'AgentList', function($scope, $interval, AgentList) {

  $scope.refresh = function() {

    AgentList().success(function(data) {
        $scope.agents = data;
        $scope.error = {
                   'show': false,
                 };
        $scope.lastupdate = new Date();
    }).error(function(err) {
       $scope.error = {
           'show': true,
           'title': 'Network error.',
           'message': 'Cannot retrieve agent list.',
         };
    });
    /*$scope.details = {
        'show': false,
      };*/
  }

  $scope.details = {
    'show': false,
  };

  $scope.getDetails = function($agent) {
    $scope.details = {
      'show': true,
      'agent': $agent,
    };
  }

  $scope.error = {
    'show': false,
  };

//  $scope.refresh();
  $interval($scope.refresh, 1000);
}]);