var app = angular.module('MapsApp', ['ngRoute']);

app.config(function ($routeProvider) {
  $routeProvider
    .when('/agents', {
      controller: 'AgentsController',
      templateUrl: 'views/agents.html'
    })
    /*.when('/agents/:id', {
      controller: 'PhotoController',
      templateUrl: 'views/photo.html'
    })*/
    .otherwise({
      redirectTo: '/agents'
    });
});