package it.rse.util.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility for plan date unmarshalling.
 *
 * @author Carlo Sandroni
 * @version 1.0, 09/09/2015
 */

public class PlanDateAdapter extends XmlAdapter<String, Date> {

  private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

  @Override
  public String marshal(Date date) throws Exception {
    return dateFormat.format(date);
  }

  @Override
  public Date unmarshal(String string) throws Exception {
    return dateFormat.parse(string);
  }

}
