package it.rse.util.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Utility for datetime unmarshalling.
 *
 * @author Carlo Sandroni
 * @version 1.0, 08/09/2015
 */

public class LoadDateAdapter extends XmlAdapter<String, Date> {

  private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  @Override
  public String marshal(Date date) throws Exception {
    return dateFormat.format(date);
  }

  @Override
  public Date unmarshal(String string) throws Exception {
    return dateFormat.parse(string);
  }

}
