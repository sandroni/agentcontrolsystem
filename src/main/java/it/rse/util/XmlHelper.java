package it.rse.util;

import it.rse.retiattive.acs.comm.MessageLanguage;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4SafeDecompressor;
import org.jetbrains.annotations.NotNull;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * Classe di utilità per semplificare la serializzazione XML.
 * TODO: rivedere compressione messaggi
 *
 * @author Carlo Sandroni
 * @version 1.0, 31/10/2014
 */
@SuppressWarnings("unchecked")
public class XmlHelper {
  private static final int MAX_DECOMPRESSED_LENGTH = 10000;

  /**
   * Serialize in XML the object <em>object</em> of the class <em>T</em>.
   *
   * @param object object to be serialized.
   * @param <T>    class of the object implementing MapsSerializable.
   * @return string representation of the object in XML.
   * @throws JAXBException if an error was encountered in the serialization process
   * @see MapsSerializable
   */
  public static <T extends MapsSerializable> String toXml(T object) throws JAXBException {
    return toXml(object, object.getClass());
  }

  /**
   * Serialize in XML the object <em>object</em> of the class <em>T</em> specified in <em>clazz</em>.
   *
   * @param object object to be serialized.
   * @param clazz  class of the object.
   * @param <T>    class of the object implementing MapsSerializable.
   * @return string representation of the object in XML.
   * @throws JAXBException if an error was encountered in the serialization process
   * @see MapsSerializable
   */
  public static <T extends MapsSerializable> String toXml(MapsSerializable object,
                                                          Class<T> clazz) throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    Writer stringWriter = new StringWriter();
    jaxbMarshaller.marshal(object, stringWriter);
    return stringWriter.toString();
  }

  /**
   * Deserialize an object from an XML representation from a string.
   * It supports the enconding languages defined in MessageLanguage.
   *
   * @param xmlString string containing the XML representation
   * @param clazz     class of the object to be deserialized
   * @param lang      encoding language as defined in MessageLanguage
   * @param <T>       class of the object to be deserialized, implementing MapsSerializable
   * @return the deserialized object
   * @throws JAXBException if an error was encountered in the deserialization process
   * @see MessageLanguage
   * It can deserialize objects implementing MapsSerializable interface.
   * @see MapsSerializable
   */
  public static <T extends MapsSerializable> T fromXml(String xmlString,
                                                       Class<T> clazz,
                                                       String lang) throws JAXBException {
    if (MessageLanguage.XML_LZ4.equals(lang)) {
      try {
//                System.out.println("Trying to decompress...");
//                System.out.println(xmlString);
        LZ4Factory factory = LZ4Factory.fastestInstance();
        LZ4SafeDecompressor decompressor = factory.safeDecompressor();
        byte[] restored = new byte[MAX_DECOMPRESSED_LENGTH];
        byte[] compressed = xmlString.getBytes("UTF-8");
        int compressedLength = compressed.length;
        int decompressedLength = decompressor.decompress(compressed, 0, compressedLength, restored, 0);
        return fromXml(new String(restored, "UTF-8"), clazz);
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
      return null;
    } else {
//            System.out.println("Not compressed...");
      return fromXml(xmlString, clazz);
    }
  }

  /**
   * Deserialize an object from an XML representation from a string.
   * It supports the only plain text enconding.
   * It can deserialize objects implementing MapsSerializable interface.
   *
   * @param xmlString string containing the XML representation
   * @param clazz     class of the object to be deserialized
   * @param <T>       class of the object to be deserialized, implementing MapsSerializable
   * @return the deserialized object
   * @throws JAXBException if an error was encountered in the deserialization process
   * @see MapsSerializable
   */
  @NotNull
  public static <T extends MapsSerializable> T fromXml(String xmlString, Class<T> clazz) throws JAXBException {
    return (T) getUnmarshaller(clazz).unmarshal(new StringReader(xmlString));
  }

  /**
   * Deserialize an object from an XML representation from a file.
   * It supports the only plain text enconding.
   * It can deserialize objects implementing MapsSerializable interface.
   *
   * @param file  file resource containing the XML
   * @param clazz class of the object to be deserialized
   * @param <T>   class of the object to be deserialized, implementing MapsSerializable
   * @return the deserialized object
   * @throws JAXBException if an error was encountered in the deserialization process
   * @see MapsSerializable
   */
  public static <T extends MapsSerializable> T fromXml(java.io.File file, Class<T> clazz) throws JAXBException {
    return (T) getUnmarshaller(clazz).unmarshal(file);
  }

  /**
   * Deserialize an object from an XML representation from an input stream.
   * It supports the only plain text enconding.
   * It can deserialize objects implementing MapsSerializable interface.
   *
   * @param inputStream input stream containing the XML
   * @param clazz       class of the object to be deserialized
   * @param <T>         class of the object to be deserialized, implementing MapsSerializable
   * @return the deserialized object
   * @throws JAXBException if an error was encountered in the deserialization process
   * @see MapsSerializable
   */
  public static <T extends MapsSerializable> T fromXml(java.io.InputStream inputStream,
                                                       Class<T> clazz) throws JAXBException {
    return (T) getUnmarshaller(clazz).unmarshal(inputStream);
  }

  /**
   * Deserialize an object from an XML representation from an URL.
   * It supports the only plain text enconding.
   * It can deserialize objects implementing MapsSerializable interface.
   *
   * @param url   URL containing the XML
   * @param clazz class of the object to be deserialized
   * @param <T>   class of the object to be deserialized, implementing MapsSerializable
   * @return the deserialized object
   * @throws JAXBException if an error was encountered in the deserialization process
   * @see MapsSerializable
   */
  public static <T extends MapsSerializable> T fromXml(java.net.URL url, Class<T> clazz) throws JAXBException {
    return (T) getUnmarshaller(clazz).unmarshal(url);
  }

  /**
   * Create an Unmarshaller object for the given class implementing MapsSerializable.
   *
   * @param clazz class of the object to be deserialized
   * @param <T>   class of the object to be deserialized, implementing MapsSerializable
   * @return Unmarshaller for the given class
   * @throws JAXBException if an error was encountered in the Unmarshaller creation
   * @see MapsSerializable
   */
  private static <T extends MapsSerializable> Unmarshaller getUnmarshaller(Class<T> clazz) throws JAXBException {
    JAXBContext jaxbQueryContext = JAXBContext.newInstance(clazz);
//        System.out.println(jaxbQueryContext.getClass());
    return jaxbQueryContext.createUnmarshaller();
  }

}
