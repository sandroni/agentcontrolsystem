package it.rse.util;

import it.rse.retiattive.acs.comm.MessageLanguage;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;

import java.io.UnsupportedEncodingException;
import javax.xml.bind.JAXBException;

/**
 * Interfaccia da implementare nelle classi che supportano la serializzazione.
 *
 * @author Carlo Sandroni
 * @version 1.0, 31/10/2014
 */
public interface MapsSerializable {

  /**
   * Serialize the object to XML.
   *
   * @return Plain string with XML representation.
   * @throws JAXBException if the XML encoding fails.
   */
  default String toXml() throws JAXBException {
    return XmlHelper.toXml(this);
  }

  /**
   * Serialize the object to XML and compress it in LZ4.
   *
   * @return Compressed XML representation.
   * @throws JAXBException if the XML encoding fails.
   */
  default String toLZ4CompressedXml() throws JAXBException {
    try {
      byte[] data = toXml().getBytes("UTF-8");
      final int decompressedLength = data.length;
      LZ4Factory factory = LZ4Factory.fastestInstance();
      LZ4Compressor compressor = factory.fastCompressor();
      int maxCompressedLength = compressor.maxCompressedLength(decompressedLength);
      byte[] compressed = new byte[maxCompressedLength];
      int compressedLength = compressor.compress(data, 0, decompressedLength, compressed, 0, maxCompressedLength);
      System.out.printf("Compressed %d data in %d output", decompressedLength, compressedLength);
      return new String(compressed, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return "";
  }

  /**
   * Serialize a message with the selected language.
   *
   * @param lang language to be used
   * @return String representing the serialized object
   * @throws JAXBException if the XML serialization fails
   */
  default String serialize(String lang) throws JAXBException {
    if (MessageLanguage.XML_LZ4.equals(lang)) {
      return toLZ4CompressedXml();
    } else {
      return toXml();
    }
  }
}
