package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.domain.TimeInfo;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ServicesHelper;
import it.rse.retiattive.acs.service.TimeService;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Agente di temporizzazione.
 *
 * @author Carlo Sandroni
 * @version 1.0, 20/11/2014
 */
public class Ticker extends Agent {
  protected transient Logger logger;
  public static final String MAPS_SERVICE_NAME = "maps-time-service";
  public static final String SERVICE_NAME = "time-service";
  private MapsOntologyHelper<TimeInfo> timeTopicHelper = new MapsOntologyHelper<>(TimeInfo.TOPIC_NAME);
  private static final String configFilename = "maps.properties";
  private long timeBase;

  @Override
  protected void setup() {
    logger = Logger.getMyLogger(getName());

    AcsService timeService = new TimeService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, timeService, mapsService);
      timeTopicHelper.createTopic(this);
      config();
      long timetostart = timeBase - Math.floorMod(System.currentTimeMillis(), timeBase);
      if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "Waiting " + timetostart + "ms to start...");
      }
      addBehaviour(new WakerBehaviour(this, timetostart) {
        @Override
        protected void onWake() {
          addBehaviour(new BaseTimeTicker(Ticker.this, timeBase));
          stop();
        }
      });
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }

  public class BaseTimeTicker extends TickerBehaviour {
    public BaseTimeTicker(Agent agent, long period) {
      super(agent, period);
    }

    @Override
    public void onStart() {
      super.onStart();
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "Ticker inizializzato: " + getPeriod() + "ms");
      }
      this.setFixedPeriod(true);
    }

    @Override
    protected void onTick() {
      TimeInfo time = new TimeInfo();
      ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
      try {
        timeTopicHelper.encodeTopicMessage(msg, time);
      } catch (MapsException e) {
        e.printStackTrace();
      }
      send(msg);
    }
  }

  private void config() throws MapsException {
    Properties prop = new Properties();
    try (InputStream input = getClass().getClassLoader().getResourceAsStream(configFilename)) {
      if (input != null) {
        prop.load(input);
        timeBase = Long.valueOf(prop.getProperty("time.base"));
      } else {
        throw new IOException("Config file not found");
      }
    } catch (IOException e) {
      throw new MapsException("MAPS Configuration error", e);
    }
  }

  @Override
  protected void takeDown() {
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}