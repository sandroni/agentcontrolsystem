package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.domain.AgentDescription;
import it.rse.retiattive.acs.domain.AgentDescriptionList;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.CreationService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ServicesHelper;
import it.rse.util.XmlHelper;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Done;
import jade.core.AID;
import jade.core.Agent;
import jade.core.ContainerID;
import jade.core.ServiceException;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.CreateAgent;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.lang.acl.ACLMessage;
import jade.proto.SimpleAchieveREInitiator;
import jade.util.Logger;
import jade.wrapper.ControllerException;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.bind.JAXBException;

/**
 * Agente di creazione.
 * L'agente legge un file XML di configurazione e invoca l'AMS per la creazione di tutti gli agenti descritti.
 *
 * @author Carlo Sandroni
 * @version 2.0, 24/07/2015
 * @since 1.0
 */
public class Creator extends Agent {
  protected transient Logger logger;
  public static final String MAPS_SERVICE_NAME = "maps-creation-service";
  public static final String SERVICE_NAME = "creation-service";
  private final MapsOntologyHelper<AgentDescription> agentDescriptionHelper
      = new MapsOntologyHelper<>(AgentDescription.ONTOLOGY_NAME);
  private AgentDescriptionList agList;
  private Map<String, AgentDescription> agMap;
  private Map<String, Boolean> created = new HashMap<>();

  @Override
  protected void setup() {
    logger = Logger.getMyLogger(getName());

    /** Add Creation Service. */
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    AcsService creationService = new CreationService(SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, creationService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }

    getContentManager().registerOntology(JADEManagementOntology.getInstance());
    getContentManager().registerLanguage(new SLCodec(), FIPANames.ContentLanguage.FIPA_SL0);

    String agentFileName = "old/agents.xml";
    Object[] args = getArguments();
    if (args != null && args.length > 0) {
      agentFileName = args[0].toString();
    }

    if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "Apertura file " + agentFileName + "...");
    }
    InputStream agentFileStream = getClass().getClassLoader().getResourceAsStream(agentFileName);
    if (agentFileStream != null) {
      try {
        agList = XmlHelper.fromXml(agentFileStream, AgentDescriptionList.class);
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "File " + agentFileName + " caricato.");
        }
        if (agList == null || agList.length() == 0) {
          throw new MapsException("Nessun agente trovato.");
        }
        agMap = agList.getNamesMap();
        for (String agentName : agMap.keySet()) {
          created.put(agentName, false);
        }
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "Trovati " + agList.length() + " agenti.");
        }
        final Iterator<CreateAgent> caIter = agList.getCreateAgentList().iterator();
        addBehaviour(new SimpleBehaviour() {
          @Override
          public void action() {
            if (caIter.hasNext()) {
              CreateAgent ca = caIter.next();
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "Creazione dell'agente " + ca.getAgentName() + " in corso...");
              }
              newAgent(ca);
            }
          }

          @Override
          public boolean done() {
            return (!caIter.hasNext());
          }
        });
      } catch (JAXBException | MapsException e) {
        if (logger.isLoggable(Logger.SEVERE)) {
          logger.log(Logger.SEVERE, "Formato file non supportato.");
        }
        e.printStackTrace();
      }
    } else {
      if (logger.isLoggable(Logger.SEVERE)) {
        logger.log(Logger.SEVERE, "File agenti " + agentFileName + " non trovato o non accessibile.");
      }
    }
  }

  /**
   * Invoca l'AMS per la creazione di un singolo agente.
   *
   * @param ca Descrizione dell'agente da creare
   */
  public void newAgent(CreateAgent ca) {
    // fill the create action with the intended agent owner
    jade.security.JADEPrincipal intendedOwner = null;
    if (ca.getOwner() == null) {
      // it is requested the creation of an agent with the same owner of the AgentCreator
      try {
        jade.security.JADEPrincipal myOwner = null;
        jade.security.Credentials myCredentials = null;
        jade.security.CredentialsHelper ch
            = (jade.security.CredentialsHelper) getHelper("jade.core.security.Security");
        // get AgentCreator's owner
        if (ch != null) {
          myCredentials = ch.getCredentials();
        }
        if (myCredentials != null) {
          myOwner = myCredentials.getOwner();
        }
        intendedOwner = myOwner;
      } catch (ServiceException se) { // Security service not present. Owner is null.
        intendedOwner = null;
      }
    }
    ca.setOwner(intendedOwner);
    try {
      ca.setContainer(new ContainerID(this.getContainerController().getContainerName(), null));
    } catch (ControllerException e) {
      e.printStackTrace();
    }
    //ca.setContainer(new ContainerID("pippo", (TransportAddress)null));

    try {
      Action action = new Action();
      action.setActor(getAMS());
      action.setAction(ca);

      ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST);
      requestMsg.setSender(getAID());
      requestMsg.addReceiver(getAMS());
      requestMsg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
      requestMsg.setLanguage(FIPANames.ContentLanguage.FIPA_SL0);

      requestMsg.setOntology(JADEManagementOntology.NAME);
      getContentManager().fillContent(requestMsg, action);
      addBehaviour(new AmsClientBehaviour("CreateAgent", requestMsg));
    } catch (Exception fe) {
      fe.printStackTrace();
    }

  }


  private class AmsClientBehaviour extends SimpleAchieveREInitiator {
    private String actionName;

    public AmsClientBehaviour(String an, ACLMessage request) {
      super(Creator.this, request);
      actionName = an;
    }

    protected void handleNotUnderstood(ACLMessage reply) {
      if (logger.isLoggable(Logger.SEVERE)) {
        logger.log(Logger.SEVERE,
            "NOT-UNDERSTOOD received by RMA during " + actionName + "\n" + reply);
      }
    }

    protected void handleRefuse(ACLMessage reply) {
      if (logger.isLoggable(Logger.SEVERE)) {
        logger.log(Logger.SEVERE,
            "REFUSE received during " + actionName + "\n" + reply);
      }
    }

    protected void handleAgree(ACLMessage reply) {
      if (logger.isLoggable(Logger.FINE)) {
        logger.log(Logger.FINE, "AGREE received" + reply);
      }
    }

    protected void handleFailure(ACLMessage reply) {
      if (logger.isLoggable(Logger.SEVERE)) {
        logger.log(Logger.SEVERE,
            "FAILURE received during " + actionName + "\n" + reply);
      }
    }

    protected void handleInform(ACLMessage reply) {
      if (logger.isLoggable(Logger.FINE)) {
        logger.log(Logger.FINE, "INFORM received" + reply);
      }
      try {
        ContentElement ce = getContentManager().extractContent(reply);
        if (ce instanceof Done) {
          Done doneReply = (Done) ce;
          Concept doneAction = doneReply.getAction();
          if (doneAction instanceof Action) {
            Action actionReply = (Action) doneAction;
            Concept actionReply2 = actionReply.getAction();
            if (actionReply2 instanceof CreateAgent) {
              CreateAgent createReply = (CreateAgent) actionReply2;
              String newAgentName = createReply.getAgentName();
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "Agente " + newAgentName + " creato.");
              }
              AgentDescription agDesc;
              if (agMap != null && (agDesc = agMap.get(newAgentName)) != null) {
                ACLMessage descriptionMsg = new ACLMessage(ACLMessage.INFORM);
                descriptionMsg.addReceiver(new AID(newAgentName, AID.ISLOCALNAME));
                agentDescriptionHelper.encodeMessage(descriptionMsg, agDesc);
                send(descriptionMsg);
              }

              created.put(newAgentName, true);
              if (!created.values().contains(false)) {
                if (logger.isLoggable(Logger.INFO)) {
                    logger.log(Logger.INFO, "Tutti gli agenti richiesti sono stati creati. Muoio.");
                }
                doDelete();
              }
            }
          }
        }
      } catch (Codec.CodecException | MapsException | OntologyException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  protected void takeDown() {
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}
