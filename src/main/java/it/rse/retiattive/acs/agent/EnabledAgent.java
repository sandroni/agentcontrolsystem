package it.rse.retiattive.acs.agent;

/**
 * Interfaccia per agenti abilitabili.
 * Definisce i callback onEnable() e onDisable().
 *
 * @author Carlo Sandroni
 * @version 2.0, 27/07/2015
 */
public interface EnabledAgent {
  /**
   * Metodo di callback su segnale di enable remoto
   *
   * @param sender Nome del mittente.
   * @return enable procedure result
   */
  boolean onEnable(String sender);

  /**
   * Metodo di callback su segnale di disable remoto
   *
   * @param sender Nome del mittente.
   * @return disable procedure result
   */
  boolean onDisable(String sender);
}
