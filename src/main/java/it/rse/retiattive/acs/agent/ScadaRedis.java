package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.behaviour.EnableListener;
import it.rse.retiattive.acs.behaviour.MessageArrivalCallback;
import it.rse.retiattive.acs.behaviour.MessageListener;
import it.rse.retiattive.acs.comm.DataMessage;
import it.rse.retiattive.acs.domain.StateVariable;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ScadaService;
import it.rse.retiattive.acs.service.ServicesHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.util.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * SCADA Agent.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/07/2015
 */
public class ScadaRedis extends Agent implements EnabledAgent {
  public static final String MAPS_SERVICE_NAME = "maps-scada-service";
  public static final String SERVICE_NAME = "scada-centralized-redis-service";
  private static final String redisConfigFilename = "redis.properties";

  private JedisPool jedisPool;
  protected transient Logger logger;
  private final MapsOntologyHelper<DataMessage> dataMessageHelper
      = new MapsOntologyHelper<>(DataMessage.ONTOLOGY_NAME);
  private boolean enabled;
  private String hostname;
  private Integer port;
  private String measuresHash;
  private String setpointHash;

  @Override
  protected void setup() {
    logger = Logger.getMyLogger(getName());
    enabled = false;

    try {
      readConfig();
      jedisPool = new JedisPool(new JedisPoolConfig(), hostname, port);
    } catch (final MapsException e) {
      if (logger.isLoggable(Logger.SEVERE)) {
        logger.log(Logger.SEVERE, "Can't connect to redis server. I'm dying...");
      }
      doDelete();
    }

    addBehaviour(new EnableListener(this));

    addBehaviour(new MessageListener<>(
        this,
        DataMessage.class,
        new MessageArrivalCallback<DataMessage>() {
          @Override
          public void onMessage(DataMessage dm, AID sender, ACLMessage reply) {
            if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "Messaggio arrivato da " + sender.getLocalName() + ": " + dm.toString());
            }
            if (enabled) {
              if (dm.getAction().equals(DataMessage.Action.GET)) {

                try {
                  Jedis jedisClient = jedisPool.getResource();
                  for (StateVariable sv : dm.getVarList()) {
                    String value = jedisClient.hget(measuresHash, sv.getName());
                    if (value != null) {
                      sv.setValue(Double.valueOf(value));
                    } else {
                      if (logger.isLoggable(Logger.SEVERE)) {
                        logger.log(Logger.SEVERE, "Error: variable " + sv.getName() + " not found.");
                      }
                    }
                  }
                  jedisClient.close();
                } catch (JedisConnectionException e) {
                  if (logger.isLoggable(Logger.SEVERE)) {
                    logger.log(Logger.SEVERE, "Can't connect to redis server. I'm dying...");
                  }
                  doDelete();
                }

                dm.setAction(DataMessage.Action.TELL);
                try {
                  reply.setPerformative(ACLMessage.INFORM);
                  dataMessageHelper.encodeMessage(reply, dm);
                  send(reply);
                } catch (MapsException e) {
                  e.printStackTrace();
                }
              } else if (dm.getAction().equals(DataMessage.Action.SET)) {
                try {
                  Jedis jedisClient = jedisPool.getResource();
                  for (StateVariable sv : dm.getVarList()) {
                    Long redisReply = jedisClient.hset(setpointHash, sv.getName(), sv.getValue().toString());
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "Variabile " + sv.getName() + " set to " + sv.getValue());
                    }
                  }
                  jedisClient.close();

                  dm.setAction(DataMessage.Action.TELL);
                  reply.setPerformative(ACLMessage.CONFIRM);
                  dataMessageHelper.encodeMessage(reply, dm);
                  send(reply);

                } catch (JedisConnectionException e) {
                  if (logger.isLoggable(Logger.SEVERE)) {
                    logger.log(Logger.SEVERE, "Can't connect to redis server. I'm dying...");
                  }
                  doDelete();
                } catch (MapsException e) {
                  e.printStackTrace();
                  doDelete();
                }
              }
            }
          }
        }
    ));

    AcsService scadaService = new ScadaService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, scadaService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "[" + getLocalName() + "] Agente pronto.");
    }
  }

  /**
   * Metodo di callback su segnale di enable remoto
   *
   * @param sender Nome del mittente.
   * @return enable procedure result
   */
  @Override
  public boolean onEnable(String sender) {
    enabled = true;
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "[" + getLocalName() + "] Abilitato da " + sender);
    }
    return true;
  }

  /**
   * Metodo di callback su segnale di disable remoto
   *
   * @param sender Nome del mittente.
   * @return disable procedure result
   */
  @Override
  public boolean onDisable(String sender) {
    enabled = false;
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "[" + getLocalName() + "] Disabilitato da " + sender);
    }
    return true;
  }

  @Override
  protected void takeDown() {
    if (jedisPool != null) {
      jedisPool.destroy();
    }
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }

  private void readConfig() throws MapsException {
    Properties prop = new Properties();
    try (InputStream input = getClass().getClassLoader().getResourceAsStream(redisConfigFilename)) {
      if (input != null) {
        prop.load(input);
        hostname = prop.getProperty("redis.server.hostname");
        port = Integer.valueOf(prop.getProperty("redis.server.port"));
        measuresHash = prop.getProperty("redis.measures.hash");
        setpointHash = prop.getProperty("redis.setpoints.hash");
      } else {
        throw new IOException("Config file not found");
      }
    } catch (IOException e) {
      throw new MapsException("MAPS Configuration error", e);
    }
  }

}