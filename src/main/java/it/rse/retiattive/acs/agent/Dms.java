package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.behaviour.MessageReplyCallback;
import it.rse.retiattive.acs.behaviour.PerformMessage;
import it.rse.retiattive.acs.comm.EnableMessage;
import it.rse.retiattive.acs.domain.AgentInfo;
import it.rse.retiattive.acs.domain.AgentStatus;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.DmsService;
import it.rse.retiattive.acs.service.EnergyTrackerService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ScadaService;
import it.rse.retiattive.acs.service.ScheduleService;
import it.rse.retiattive.acs.service.ServicesHelper;
import it.rse.retiattive.acs.ws.WebServerHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.util.Logger;
import jade.util.leap.Iterator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Distribution Management System agent..
 * TODO: support for multiple ET and SCADA, fix subscription, modify enable process
 *
 * @author Carlo Sandroni
 * @version 2.0, 24/07/2015
 */
public class Dms extends Agent {
  public static final String MAPS_SERVICE_NAME = "maps-dms-service";
  public static final String SERVICE_NAME = "dms-service";
  public static final int TIMEOUT = 1000;
  protected transient Logger logger;
  protected AID energyTrackerAid;
  protected AID scadaAid;
  protected List<AID> schedulesAid;
  private WebServerHelper ws;
  private Map<String, AgentInfo> mapsAgents = new HashMap<>();

  @Override
  protected void setup() {
    logger = Logger.getMyLogger(getName());

    /** Add DMS Service. */
    AcsService dmsService = new DmsService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, dmsService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }

    /** Subscribing to MAPS services lookup. */
    ServicesHelper.subscribeService(
        this,
        MapsService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        if (serviceProviders.length > 0) {
          for (DFAgentDescription sp : serviceProviders) {
            mapsAgents.remove(sp.getName().getLocalName());
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(MapsService.SERVICE_TYPE)) {
                AgentInfo status = new AgentInfo(sp);
                mapsAgents.put(status.getName(), status);
              }
            }
          }
        }
        if (mapsAgents.size() > 0) {
          if (logger.isLoggable(Logger.INFO)) {
            logger.log(Logger.INFO, "Currently there are " + mapsAgents.size() + " Maps Agents online.");
          }
        } else {
          if (logger.isLoggable(Logger.WARNING)) {
            logger.log(Logger.WARNING, "Currently there are no Maps Agents online.");
          }
        }
      }
    );


    /** Enable EnergyTracker. */
    ServicesHelper.subscribeService(
        this,
        EnergyTrackerService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        // wrong indentation, checkstyle bug
        if (serviceProviders.length > 0) {
          energyTrackerAid = null;
        providers:
          for (DFAgentDescription sp : serviceProviders) {
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(EnergyTrackerService.SERVICE_TYPE)) {
                energyTrackerAid = sp.getName();
                break providers;
              }
            }
          }
          if (energyTrackerAid != null) {
            if (logger.isLoggable(Logger.INFO)) {
              logger.log(Logger.INFO, "EnergyTracker found: " + energyTrackerAid.getLocalName());
            }
            EnableMessage enableMsg = new EnableMessage(EnableMessage.Action.ENABLE);
            addBehaviour(new PerformMessage<>(
                Dms.this,
                energyTrackerAid,
                enableMsg,
                new MessageReplyCallback<EnableMessage>() {
                  @Override
                  public void onTimeout(EnableMessage message, String info) {
                    mapsAgents.get(energyTrackerAid.getLocalName()).setStatus(AgentStatus.ERROR);
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message timeout");
                    }
                  }

                  @Override
                  public void onSuccess(EnableMessage message, String info) {
                    mapsAgents.get(energyTrackerAid.getLocalName()).setStatus(AgentStatus.ENABLED);
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message success");
                    }
                  }

                  @Override
                  public void onFail(EnableMessage message, String info) {
                    mapsAgents.get(energyTrackerAid.getLocalName()).setStatus(AgentStatus.ERROR);
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message fail");
                    }
                  }
                },
                TIMEOUT
            ));
          } else {
            if (logger.isLoggable(Logger.WARNING)) {
              logger.log(Logger.WARNING, "EnergyTracker not found.");
            }
          }
        }
      }
    );

    /** Enable SCADA. */
    ServicesHelper.subscribeService(
        this,
        ScadaService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        // wrong indentation, checkstyle bug
        if (serviceProviders.length > 0) {
          scadaAid = null;
        providers:
          for (DFAgentDescription sp : serviceProviders) {
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(ScadaService.SERVICE_TYPE)) {
                scadaAid = sp.getName();
                break providers;
              }
            }
          }
          if (scadaAid != null) {
            if (logger.isLoggable(Logger.INFO)) {
              logger.log(Logger.INFO, "SCADA found: " + scadaAid.getLocalName());
            }
            EnableMessage enableMsg = new EnableMessage(EnableMessage.Action.ENABLE);
            addBehaviour(new PerformMessage<>(
                Dms.this,
                scadaAid,
                enableMsg,
                new MessageReplyCallback<EnableMessage>() {
                  @Override
                  public void onTimeout(EnableMessage message, String info) {
                    mapsAgents.get(scadaAid.getLocalName()).setStatus(AgentStatus.ERROR);
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message timeout");
                    }
                  }

                  @Override
                  public void onSuccess(EnableMessage message, String info) {
                    mapsAgents.get(scadaAid.getLocalName()).setStatus(AgentStatus.ENABLED);
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message success");
                    }
                  }

                  @Override
                  public void onFail(EnableMessage message, String info) {
                    mapsAgents.get(scadaAid.getLocalName()).setStatus(AgentStatus.ERROR);
                    if (logger.isLoggable(Logger.INFO)) {
                      logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message fail");
                    }
                  }
                },
                TIMEOUT
            ));
          } else {
            if (logger.isLoggable(Logger.WARNING)) {
              logger.log(Logger.WARNING, "Scada not found.");
            }
          }
        }
      }
    );



    /** Enable Schedulers. */
    ServicesHelper.subscribeService(
        this,
        ScheduleService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        if (serviceProviders.length > 0) {
          schedulesAid = new ArrayList<>();
          for (DFAgentDescription sp : serviceProviders) {
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(ScheduleService.SERVICE_TYPE)) {
                schedulesAid.add(sp.getName());
              }
            }
          }
          if (schedulesAid != null && !schedulesAid.isEmpty()) {
            if (logger.isLoggable(Logger.INFO)) {
              logger.log(Logger.INFO, "Schedulers found: " + schedulesAid);
            }
            for (AID schedule : schedulesAid) {
              EnableMessage enableMsg = new EnableMessage(EnableMessage.Action.ENABLE);
              addBehaviour(new PerformMessage<>(
                  Dms.this,
                  schedule,
                  enableMsg,
                  new MessageReplyCallback<EnableMessage>() {
                    @Override
                    public void onTimeout(EnableMessage message, String info) {
                      mapsAgents.get(schedule.getLocalName()).setStatus(AgentStatus.ERROR);
                      if (logger.isLoggable(Logger.INFO)) {
                        logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message timeout");
                      }
                    }

                    @Override
                    public void onSuccess(EnableMessage message, String info) {
                      mapsAgents.get(schedule.getLocalName()).setStatus(AgentStatus.ENABLED);
                      if (logger.isLoggable(Logger.INFO)) {
                        logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message success");
                      }
                    }

                    @Override
                    public void onFail(EnableMessage message, String info) {
                      mapsAgents.get(schedule.getLocalName()).setStatus(AgentStatus.ERROR);
                      if (logger.isLoggable(Logger.INFO)) {
                        logger.log(Logger.INFO, "[" + Dms.this.getLocalName() + "] Enable message fail");
                      }
                    }
                  },
                  TIMEOUT
              ));
            }
          } else {
            if (logger.isLoggable(Logger.WARNING)) {
              logger.log(Logger.WARNING, "Scada not found.");
            }
          }
        }
      }
    );


    ws = new WebServerHelper(mapsAgents);
    ws.start();

    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Agente " + getLocalName() + " caricato. [" + this.getClass().getName() + "]");
    }

  }

  @Override
  protected void takeDown() {
    try {
      ws.stop();
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}