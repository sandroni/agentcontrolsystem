package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.behaviour.EnableListener;
import it.rse.retiattive.acs.behaviour.MessageReplyCallback;
import it.rse.retiattive.acs.behaviour.PerformMessage;
import it.rse.retiattive.acs.behaviour.TickListener;
import it.rse.retiattive.acs.comm.DataMessage;
import it.rse.retiattive.acs.domain.LoadProgram;
import it.rse.retiattive.acs.domain.StateVariable;
import it.rse.retiattive.acs.domain.TimeInfo;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ScadaService;
import it.rse.retiattive.acs.service.ScheduleService;
import it.rse.retiattive.acs.service.ServicesHelper;
import it.rse.util.XmlHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.util.Logger;
import jade.util.leap.Iterator;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Date;

/**
 * Generic load controller.
 *
 * @author Carlo Sandroni
 * @version 1.0, 08/09/2015
 */
public class Load extends Agent implements TimedAgent, EnabledAgent {
  public static final String MAPS_SERVICE_NAME = "maps-load-service";
  public static final String SERVICE_NAME = "load-service";
  public static final int TIMEOUT = 1000;
  protected transient Logger logger;
  public static final String loadsFileName = "./input/loads.xml";
  private File file;

  private double currentPSet;
  private double currentQSet;

  protected AID scadaAid;

  public enum Status {
    PAUSED,
    IDLE,
    ERROR,
    SENDING_DATA
  }

  private Status status;

  public Status getStatus() {
    return status;
  }

  @Override
  protected void setup() {
    status = Status.PAUSED;
    logger = Logger.getMyLogger(getName());

    file = new File(loadsFileName);
    if (file.exists()) {
      if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "File " + loadsFileName + " caricato.");
      }
    } else {
      if (logger.isLoggable(Logger.SEVERE)) {
          logger.log(Logger.SEVERE, "File " + loadsFileName + " non trovato.");
      }
      // TODO: gestione dell'errore di file not found
    }

    AcsService scheduleService = new ScheduleService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, scheduleService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }

    /** Adding behaviours. */
    addBehaviour(new TickListener(this));
    addBehaviour(new EnableListener(this));

    /** Subscribing to Scada services lookup. */
    ServicesHelper.subscribeService(
        this,
        ScadaService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        if (serviceProviders.length > 0) {
          scadaAid = null;
          providers:
          for (DFAgentDescription sp : serviceProviders) {
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(ScadaService.SERVICE_TYPE)) {
                scadaAid = sp.getName();
                break providers;
              }
            }
          }
          if (scadaAid != null) {
            if (logger.isLoggable(Logger.INFO)) {
              logger.log(Logger.INFO, "SCADA found: " + scadaAid.getLocalName());
            }
          } else {
            // TODO: go to error status
            if (logger.isLoggable(Logger.WARNING)) {
              logger.log(Logger.WARNING, "Scada not found.");
            }
          }
        }
      }
    );

    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "[" + getLocalName() + "] Agente pronto.");
    }
  }


  /**
   * Perform a status change, calling proper exit and entry activities.
   *
   * @param newStatus destination status.
   */
  public void changeStatus(Status newStatus) {
    /** Do exit status activity of current status. */
    switch (status) {
      default:
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "No exit action for status " + status);
        }
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Exited " + status + " status.");
    }
    /** Set new status. */
    this.status = newStatus;
    /** Do entry status activity of new status. */
    switch (status) {
      case SENDING_DATA:
        entrySendingData();
        break;
      // TODO: add error case
      default:
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "No enter action for status " + status);
        }
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Entered " + status + " status.");
    }
  }


  private void entrySendingData() {
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Sending Data...");
    }

    /** Lettura file. */
    LoadProgram load;
    if (file != null) {
      try {
        load = XmlHelper.fromXml(file, LoadProgram.class);
        for (LoadProgram.Node node : load.getNodes()) {
          if (node.getId() == 6) {
            Date dateToFind = new Date();
            LoadProgram.Step nearest = node.findClosestStep(dateToFind);
            currentPSet = nearest.getP();
            currentQSet = nearest.getQ();
            if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "Setpoint found: P=" + currentPSet + ", Q=" + currentQSet);
            }
          } else {
            if (logger.isLoggable(Logger.WARNING)) {
                logger.log(Logger.WARNING, "Nodo di carico sconosciuto: id=" + node.getId());
            }
          }
        }
      } catch (JAXBException e) {
        logger.severe("Formato file non supportato.");
        e.printStackTrace();
      }
    }

    /** Request SCADA. */
    if (scadaAid != null) {
      // Creazione del messaggio
      DataMessage dataMessage = new DataMessage(DataMessage.Action.SET);
      dataMessage.addVariable(new StateVariable("ctrl.Ca_Set_P1", currentPSet / 3));
      dataMessage.addVariable(new StateVariable("ctrl.Ca_Set_P2", currentPSet / 3));
      dataMessage.addVariable(new StateVariable("ctrl.Ca_Set_P3", currentPSet / 3));
      dataMessage.addVariable(new StateVariable("ctrl.Ca_Set_Q1", currentQSet / 3));
      dataMessage.addVariable(new StateVariable("ctrl.Ca_Set_Q2", currentQSet / 3));
      dataMessage.addVariable(new StateVariable("ctrl.Ca_Set_Q3", currentQSet / 3));

      addBehaviour(new PerformMessage<>(
          Load.this,
          scadaAid,
          dataMessage,
          new MessageReplyCallback<DataMessage>() {
            @Override
            public void onSuccess(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + Load.this.getLocalName() + "] Setpoint set.");
              }
              changeStatus(Status.IDLE);
            }

            @Override
            public void onTimeout(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + Load.this.getLocalName() + "] DataMessage timeout");
              }
              changeStatus(Status.ERROR);
            }

            @Override
            public void onFail(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + Load.this.getLocalName() + "] DataMessage fail");
              }
              changeStatus(Status.ERROR);
            }
          },
          TIMEOUT
      ));
    } else {
      if (logger.isLoggable(Logger.WARNING)) {
        logger.log(Logger.WARNING, "No SCADA Service available for sending data.");
      }
      changeStatus(Status.IDLE);
    }
  }


  /**
   * Metodo di callback su segnale di clock
   *
   * @param time Informazioni sull'istante temporale.
   */
  @Override
  public void onTick(TimeInfo time) {
    if (getStatus().equals(Status.IDLE)) {
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO,
            "[" + getLocalName() + "] Attivazione procedura Energy Tracker @" + time.toString());
      }
      // TODO: fetch data from xml
      changeStatus(Status.SENDING_DATA);
    }
  }

  /**
   * Metodo di callback su segnale di enable remoto
   *
   * @param sender Nome del mittente.
   * @return enable procedure result
   */
  @Override
  public boolean onEnable(String sender) {
    if (getStatus().equals(Status.PAUSED)) {
      changeStatus(Status.IDLE);
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "[" + getLocalName() + "] Abilitato da " + sender);
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Metodo di callback su segnale di disable remoto
   *
   * @param sender Nome del mittente.
   * @return disable procedure result
   */
  @Override
  public boolean onDisable(String sender) {
    if (getStatus().equals(Status.IDLE)) {
      changeStatus(Status.PAUSED);
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "[" + getLocalName() + "] Disabilitato da " + sender);
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected void takeDown() {
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}
