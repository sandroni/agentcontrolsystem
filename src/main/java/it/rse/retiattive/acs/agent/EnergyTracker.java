package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.behaviour.EnableListener;
import it.rse.retiattive.acs.behaviour.MessageReplyCallback;
import it.rse.retiattive.acs.behaviour.PerformMessage;
import it.rse.retiattive.acs.behaviour.TickListener;
import it.rse.retiattive.acs.comm.DataMessage;
import it.rse.retiattive.acs.domain.StateVariable;
import it.rse.retiattive.acs.domain.TimeInfo;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.EnergyTrackerService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ScadaService;
import it.rse.retiattive.acs.service.ServicesHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.util.Logger;
import jade.util.leap.Iterator;


/**
 * EnergyTracker control agent.
 * TODO: support for multiple SCADA, fix subscription, modify enable process
 *
 * @author Carlo Sandroni
 * @version 2.0, 24/07/2015
 */
public class EnergyTracker extends Agent implements TimedAgent, EnabledAgent {
  public static final String MAPS_SERVICE_NAME = "maps-energytracker-service";
  public static final String SERVICE_NAME = "energytracker-pi-service";
  public static final int TIMEOUT = 1000;
  protected transient Logger logger;
  protected AID scadaAid;

  public enum Status {
    PAUSED,
    IDLE,
    ERROR,
    COLLECTING_DATA,
    ELABORATING,
    SENDING_DATA
  }

  private Status status;

  public Status getStatus() {
    return status;
  }

  @Override
  protected void setup() {
    status = Status.PAUSED;
    logger = Logger.getMyLogger(getName());

    /** Registering services. */
    AcsService energyTrackerService = new EnergyTrackerService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, energyTrackerService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }

    /** Adding behaviours. */
    addBehaviour(new TickListener(this));
    addBehaviour(new EnableListener(this));

    /** Subscribing to Scada services lookup. */
    ServicesHelper.subscribeService(
        this,
        ScadaService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        if (serviceProviders.length > 0) {
          scadaAid = null;
        providers:
          for (DFAgentDescription sp : serviceProviders) {
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(ScadaService.SERVICE_TYPE)) {
                scadaAid = sp.getName();
                break providers;
              }
            }
          }
          if (scadaAid != null) {
            if (logger.isLoggable(Logger.INFO)) {
              logger.log(Logger.INFO, "SCADA found: " + scadaAid.getLocalName());
            }
          } else {
            // TODO: go to error status
            if (logger.isLoggable(Logger.WARNING)) {
              logger.log(Logger.WARNING, "Scada not found.");
            }
          }
        }
      }
    );


    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "[" + getLocalName() + "] Agente pronto.");
    }
  }

  /** Perform a status change, calling proper exit and entry activities. */
  public void changeStatus(Status newStatus) {
    /** Do exit status activity of current status. */
    switch (status) {
      default:
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "No exit action for status " + status);
        }
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Exited " + status + " status.");
    }
    /** Set new status. */
    this.status = newStatus;
    /** Do entry status activity of new status. */
    switch (status) {
      case COLLECTING_DATA:
        entryCollectingData();
        break;
      case ELABORATING:
        entryElaborating();
        break;
      case SENDING_DATA:
        entrySendingData();
        break;
      default:
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "No enter action for status " + status);
        }
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Entered " + status + " status.");
    }
  }

  /**
   * Metodo di callback su segnale di clock
   *
   * @param time Informazioni sull'istante temporale.
   */
  @Override
  public void onTick(TimeInfo time) {
    if (time.isFirstOfTracking()) {
      if (getStatus().equals(Status.IDLE)) {
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO,
              "[" + getLocalName() + "] Attivazione procedura Energy Tracker @" + time.toString());
        }
        changeStatus(Status.COLLECTING_DATA);
      }
    }
  }

  /**
   * Metodo di callback su segnale di enable remoto
   *
   * @param sender Nome del mittente.
   * @return enable procedure result
   */
  @Override
  public boolean onEnable(String sender) {
    if (getStatus().equals(Status.PAUSED)) {
      changeStatus(Status.IDLE);
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "[" + getLocalName() + "] Abilitato da " + sender);
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Metodo di callback su segnale di disable remoto
   *
   * @param sender Nome del mittente.
   * @return disable procedure result
   */
  @Override
  public boolean onDisable(String sender) {
    if (getStatus().equals(Status.IDLE)) {
      changeStatus(Status.PAUSED);
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "[" + getLocalName() + "] Disabilitato da " + sender);
      }
      return true;
    } else {
      return false;
    }
  }


  private void entryCollectingData() {
    /** Request SCADA. */
    if (scadaAid != null) {
      // Creazione del messaggio
      DataMessage dataMessage = new DataMessage(DataMessage.Action.GET);
      dataMessage.addVariable(new StateVariable("Qu.B2APt"));
      dataMessage.addVariable(new StateVariable("FV.JanPt"));

      addBehaviour(new PerformMessage<>(
          EnergyTracker.this,
          scadaAid,
          dataMessage,
          new MessageReplyCallback<DataMessage>() {
            @Override
            public void onInform(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, message.toString());
              }
              changeStatus(Status.ELABORATING);
            }

            @Override
            public void onTimeout(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + EnergyTracker.this.getLocalName() + "] DataMessage timeout");
              }
              changeStatus(Status.ERROR);
            }

            @Override
            public void onFail(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + EnergyTracker.this.getLocalName() + "] DataMessage fail");
              }
              changeStatus(Status.ERROR);
            }
          },
          TIMEOUT
      ));
    } else {
      if (logger.isLoggable(Logger.WARNING)) {
        logger.log(Logger.WARNING, "No SCADA Service available for requesting data.");
      }
      changeStatus(Status.IDLE);
    }
  }

  private void entryElaborating() {
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Elaborating...");
    }
    // Dummy activity:
    addBehaviour(new WakerBehaviour(this, 4000) {
      @Override
      protected void onWake() {
        changeStatus(Status.SENDING_DATA);
        this.stop();
      }
    });

  }

  private void entrySendingData() {
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Sending Data...");
    }
    /** Request SCADA. */
    if (scadaAid != null) {
      // Creazione del messaggio
      DataMessage dataMessage = new DataMessage(DataMessage.Action.SET);
      dataMessage.addVariable(new StateVariable("Batt1.Pset", 66.6));
      dataMessage.addVariable(new StateVariable("MTG.Pset", 33.3));

      addBehaviour(new PerformMessage<>(
          EnergyTracker.this,
          scadaAid,
          dataMessage,
          new MessageReplyCallback<DataMessage>() {
            @Override
            public void onSuccess(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + EnergyTracker.this.getLocalName() + "] Setpoint set.");
              }
              changeStatus(Status.IDLE);
            }

            @Override
            public void onTimeout(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + EnergyTracker.this.getLocalName() + "] DataMessage timeout");
              }
              changeStatus(Status.ERROR);
            }

            @Override
            public void onFail(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + EnergyTracker.this.getLocalName() + "] DataMessage fail");
              }
              changeStatus(Status.ERROR);
            }
          },
          TIMEOUT
      ));
    } else {
      if (logger.isLoggable(Logger.WARNING)) {
        logger.log(Logger.WARNING, "No SCADA Service available for sending data.");
      }
      changeStatus(Status.IDLE);
    }
  }

  @Override
  protected void takeDown() {
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}