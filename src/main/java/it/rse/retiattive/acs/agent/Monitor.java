package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.MonitorService;
import it.rse.retiattive.acs.service.ServicesHelper;
import jade.core.Agent;
import jade.util.Logger;

/**
 * Monitor Agent.
 *
 * @author Carlo Sandroni
 * @version 2.0, 24/07/2015
 */
public class Monitor extends Agent {
  public static final String MAPS_SERVICE_NAME = "maps-monitor-service";
  public static final String SERVICE_NAME = "monitor-service";
  protected transient Logger logger;

  @Override
  protected void setup() {
    logger = Logger.getMyLogger(getName());
    AcsService monitorService = new MonitorService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, monitorService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Agente " + getLocalName() + " caricato. [" + this.getClass().getName() + "]");
    }
  }

  @Override
  protected void takeDown() {
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}