package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.behaviour.EnableListener;
import it.rse.retiattive.acs.behaviour.MessageReplyCallback;
import it.rse.retiattive.acs.behaviour.PerformMessage;
import it.rse.retiattive.acs.behaviour.TickListener;
import it.rse.retiattive.acs.comm.DataMessage;
import it.rse.retiattive.acs.domain.PlanProgram;
import it.rse.retiattive.acs.domain.StateVariable;
import it.rse.retiattive.acs.domain.TimeInfo;
import it.rse.retiattive.acs.service.AcsService;
import it.rse.retiattive.acs.service.MapsService;
import it.rse.retiattive.acs.service.ScadaService;
import it.rse.retiattive.acs.service.ScheduleService;
import it.rse.retiattive.acs.service.ServicesHelper;
import it.rse.util.XmlHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.util.Logger;
import jade.util.leap.Iterator;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Date;

/**
 * Send plan to selected devices.
 *
 * @author Carlo Sandroni
 * @version 1.0, 09/09/2015
 */
public class PlanSender extends Agent implements TimedAgent, EnabledAgent {
  public static final String MAPS_SERVICE_NAME = "maps-plan-service";
  public static final String SERVICE_NAME = "plan-service";
  public static final int TIMEOUT = 1000;
  protected transient Logger logger;
  public static final String planFileName = "./input/plan.xml";
  private File file;

  private Double mtg1PSet = null;
  private Double mtg1PTHSet = null;
  private Double bat1PSet = null;
  private Double bat1QSet = null;
  private Double bat1SOCSet = null;
  private Double load1PSet = null;
  private Double grid1PSet = null;

  protected AID scadaAid;

  public enum Status {
    PAUSED,
    IDLE,
    ERROR,
    SENDING_DATA
  }

  private Status status;

  public Status getStatus() {
    return status;
  }

  @Override
  protected void setup() {
    status = Status.PAUSED;
    logger = Logger.getMyLogger(getName());

    file = new File(planFileName);
    if (file.exists()) {
      if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "File " + planFileName + " caricato.");
      }
    } else {
      if (logger.isLoggable(Logger.SEVERE)) {
          logger.log(Logger.SEVERE, "File " + planFileName + " non trovato.");
      }
      // TODO: gestione dell'errore di file not found
    }

    AcsService scheduleService = new ScheduleService(SERVICE_NAME);
    AcsService mapsService = new MapsService(MAPS_SERVICE_NAME);
    try {
      ServicesHelper.registerServices(this, scheduleService, mapsService);
    } catch (MapsException e) {
      e.printStackTrace();
    }

    /** Adding behaviours. */
    addBehaviour(new TickListener(this));
    addBehaviour(new EnableListener(this));

    /** Subscribing to Scada services lookup. */
    ServicesHelper.subscribeService(
        this,
        ScadaService.SERVICE_TYPE,
        null,
        null,
        serviceProviders -> {
        if (serviceProviders.length > 0) {
          scadaAid = null;
          providers:
          for (DFAgentDescription sp : serviceProviders) {
            Iterator sdIter = sp.getAllServices();
            while (sdIter.hasNext()) {
              ServiceDescription sd = (ServiceDescription) sdIter.next();
              if (sd.getType().equals(ScadaService.SERVICE_TYPE)) {
                scadaAid = sp.getName();
                break providers;
              }
            }
          }
          if (scadaAid != null) {
            if (logger.isLoggable(Logger.INFO)) {
              logger.log(Logger.INFO, "SCADA found: " + scadaAid.getLocalName());
            }
          } else {
            // TODO: go to error status
            if (logger.isLoggable(Logger.WARNING)) {
              logger.log(Logger.WARNING, "Scada not found.");
            }
          }
        }
      }
    );

    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "[" + getLocalName() + "] Agente pronto.");
    }
  }


  /**
   * Perform a status change, calling proper exit and entry activities.
   *
   * @param newStatus destination status.
   */
  public void changeStatus(Status newStatus) {
    /** Do exit status activity of current status. */
    switch (status) {
      default:
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "No exit action for status " + status);
        }
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Exited " + status + " status.");
    }
    /** Set new status. */
    this.status = newStatus;
    /** Do entry status activity of new status. */
    switch (status) {
      case SENDING_DATA:
        entrySendingData();
        break;
      // TODO: add error case
      default:
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO, "No enter action for status " + status);
        }
    }
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Entered " + status + " status.");
    }
  }


  private void entrySendingData() {
    if (logger.isLoggable(Logger.INFO)) {
      logger.log(Logger.INFO, "Sending Data...");
    }

    /** Lettura file. */
    PlanProgram plans;
    if (file != null) {
      try {
        plans = XmlHelper.fromXml(file, PlanProgram.class);
        PlanProgram.DailyPlan plan = plans.findPlan(new Date());
        // TODO check if plan exists!!
        for (PlanProgram.Unit unit : plan.getUnits()) {
          for (PlanProgram.Attribute attribute : unit.getAttributes()) {
            PlanProgram.Item item = attribute.findClosestStep(new Date());
//            System.out.println("Unit: " + unit.getName() + "; Attribute: " + attribute.getName() + "; Item: " + item);
            if (unit.getName().equals("mtg1") && attribute.getName().equals("P")) {
              mtg1PSet = item.getValue();
            } else if (unit.getName().equals("bat1") && attribute.getName().equals("P")) {
              bat1PSet = item.getValue();
            } else if (unit.getName().equals("bat1") && attribute.getName().equals("Q")) {
              bat1QSet = item.getValue();
            }
          }
        }

      } catch (JAXBException e) {
        logger.severe("Formato file non supportato.");
        e.printStackTrace();
      }
    }



    /** Request SCADA. */
    if (scadaAid != null) {
      // Creazione del messaggio
      DataMessage dataMessage = new DataMessage(DataMessage.Action.SET);
      if (mtg1PSet != null) {
        dataMessage.addVariable(new StateVariable("ctrl.Mt_Set_Pt", mtg1PSet));
      }
      if (bat1PSet != null) {
        dataMessage.addVariable(new StateVariable("ctrl.Loc_Set_P", bat1PSet));
      }
      if (bat1QSet != null) {
        dataMessage.addVariable(new StateVariable("ctrl.Loc_Set_Q", bat1QSet));
      }

      addBehaviour(new PerformMessage<>(
          PlanSender.this,
          scadaAid,
          dataMessage,
          new MessageReplyCallback<DataMessage>() {
            @Override
            public void onSuccess(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + PlanSender.this.getLocalName() + "] Setpoint set.");
              }
              changeStatus(Status.IDLE);
            }

            @Override
            public void onTimeout(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + PlanSender.this.getLocalName() + "] DataMessage timeout");
              }
              changeStatus(Status.ERROR);
            }

            @Override
            public void onFail(DataMessage message, String info) {
              if (logger.isLoggable(Logger.INFO)) {
                logger.log(Logger.INFO, "[" + PlanSender.this.getLocalName() + "] DataMessage fail");
              }
              changeStatus(Status.ERROR);
            }
          },
          TIMEOUT
      ));
    } else {
      if (logger.isLoggable(Logger.WARNING)) {
        logger.log(Logger.WARNING, "No SCADA Service available for sending data.");
      }
      changeStatus(Status.IDLE);
    }
  }


  /**
   * Metodo di callback su segnale di clock
   *
   * @param time Informazioni sull'istante temporale.
   */
  @Override
  public void onTick(TimeInfo time) {
    if (time.isFirstOfProgram()) {
      if (getStatus().equals(Status.IDLE)) {
        if (logger.isLoggable(Logger.INFO)) {
          logger.log(Logger.INFO,
              "[" + getLocalName() + "] Attivazione procedura Applicazione piano @" + time.toString());
        }
        // TODO: fetch data from xml
        changeStatus(Status.SENDING_DATA);
      }
    }
  }

  /**
   * Metodo di callback su segnale di enable remoto
   *
   * @param sender Nome del mittente.
   * @return enable procedure result
   */
  @Override
  public boolean onEnable(String sender) {
    if (getStatus().equals(Status.PAUSED)) {
      changeStatus(Status.IDLE);
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "[" + getLocalName() + "] Abilitato da " + sender);
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Metodo di callback su segnale di disable remoto
   *
   * @param sender Nome del mittente.
   * @return disable procedure result
   */
  @Override
  public boolean onDisable(String sender) {
    if (getStatus().equals(Status.IDLE)) {
      changeStatus(Status.PAUSED);
      if (logger.isLoggable(Logger.INFO)) {
        logger.log(Logger.INFO, "[" + getLocalName() + "] Disabilitato da " + sender);
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected void takeDown() {
    try {
      ServicesHelper.deregister(this);
    } catch (MapsException e) {
      e.printStackTrace();
    }
  }
}
