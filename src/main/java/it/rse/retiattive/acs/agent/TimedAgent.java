package it.rse.retiattive.acs.agent;

import it.rse.retiattive.acs.domain.TimeInfo;

/**
 * Interfaccia per gli agenti che ricevono il segnale di clock.
 * Definisce il callback onTick().
 *
 * @author Carlo Sandroni
 * @version 1.0, 25/03/2015
 */
public interface TimedAgent {
  /**
   * Metodo di callback su segnale di clock
   *
   * @param time Informazioni sull'istante temporale.
   */
  void onTick(TimeInfo time);
}
