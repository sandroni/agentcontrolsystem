package it.rse.retiattive.acs.behaviour;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.agent.EnabledAgent;
import it.rse.retiattive.acs.comm.EnableMessage;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.Logger;

/**
 * Behaviour per gli agenti di tipo EnabledAgent che possono essere abilitati da remoto.
 *
 * @author Carlo Sandroni
 * @version 2.0, 27/7/2015
 * @see EnabledAgent
 */
public class EnableListener extends Behaviour {
  public static final Logger logger = Logger.getMyLogger(EnableListener.class.getName());
  private final MapsOntologyHelper<EnableMessage> enableMessageHelper
      = new MapsOntologyHelper<>(EnableMessage.ONTOLOGY_NAME);
  private MessageTemplate enableMt;
  private boolean error = false;
  EnabledAgent myEnabledAgent;

  /**
   * Default constructor.
   *
   * @param agent EnabledAgent that owns the behaviour.
   */
  public EnableListener(EnabledAgent agent) {
    super((Agent) agent);
    try {
      if (agent == null) {
        throw new MapsException("L'agente deve implementare l'interfaccia EnabledAgent");
      }
      myEnabledAgent = agent;
      enableMt = enableMessageHelper.createMessageTemplate(ACLMessage.REQUEST);
    } catch (MapsException e) {
      e.printStackTrace();
      error = true;
    }
  }

  @Override
  public void action() {
    if (!error) {
      ACLMessage msg = myAgent.receive(enableMt);
      if (msg != null) {
        try {
          EnableMessage enable = enableMessageHelper.decodeMessage(msg, EnableMessage.class);
          boolean success;
          if (enable.getAction().equals(EnableMessage.Action.ENABLE)) {
            success = myEnabledAgent.onEnable(msg.getSender().getName());
          } else if (enable.getAction().equals(EnableMessage.Action.DISABLE)) {
            success = myEnabledAgent.onDisable(msg.getSender().getName());
          } else {
            success = false;
          }
          ACLMessage replyEnableMsg = msg.createReply();
          if (success) {
            replyEnableMsg.setPerformative(ACLMessage.CONFIRM);
          } else {
            replyEnableMsg.setPerformative(ACLMessage.FAILURE);
          }
          myAgent.send(replyEnableMsg);
        } catch (MapsException e) {
          e.printStackTrace();
        }
      } else {
        block();
      }
    }
  }

  @Override
  public boolean done() {
    return error;
  }
}
