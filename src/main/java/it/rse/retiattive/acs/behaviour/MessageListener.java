package it.rse.retiattive.acs.behaviour;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.comm.EnableMessage;
import it.rse.retiattive.acs.comm.GenericMessage;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.Logger;

/**
 * Behaviour per gli agenti che attendono dei messaggi inviati con PerformMessage.
 *
 * @author Carlo Sandroni
 * @version 2.0, 29/7/2015
 * @see PerformMessage
 * @see it.rse.retiattive.acs.comm.GenericMessage
 */
public class MessageListener<T extends GenericMessage> extends Behaviour {
  public static final Logger logger = Logger.getMyLogger(MessageListener.class.getName());
  private MapsOntologyHelper<T> messageHelper;
  private MessageTemplate genericMt;
  private boolean error = false;
  private Class<T> tclass;
  MessageArrivalCallback<T> callback;

  /**
   * Default constructor.
   *
   * @param agent EnabledAgent that owns the behaviour.
   */
  public MessageListener(Agent agent, Class<T> tclass, MessageArrivalCallback<T> callback) {
    super(agent);
    this.tclass = tclass;
    this.callback = callback;
    try {
      messageHelper = new MapsOntologyHelper<T>(tclass.newInstance().getOntologyName());
      genericMt = messageHelper.createMessageTemplate(ACLMessage.REQUEST);
    } catch (InstantiationException | IllegalAccessException e) {
      error = true;
      e.printStackTrace();
    }
  }

  @Override
  public void action() {
    if (!error && messageHelper != null) {
      ACLMessage msg = myAgent.receive(genericMt);
      if (msg != null) {
        try {
          T decodedMessage = messageHelper.decodeMessage(msg, tclass);
          ACLMessage replyMsg = msg.createReply();
          callback.onMessage(decodedMessage, msg.getSender(), replyMsg);
        } catch (MapsException e) {
          e.printStackTrace();
        }
      } else {
        block();
      }
    }
  }

  @Override
  public boolean done() {
    return error;
  }
}
