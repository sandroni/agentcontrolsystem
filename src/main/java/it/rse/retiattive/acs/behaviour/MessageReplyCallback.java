package it.rse.retiattive.acs.behaviour;

import it.rse.retiattive.acs.comm.GenericMessage;
import it.rse.retiattive.acs.ontology.MapsOntology;

/**
 * Interfaccia per le classi che inviano richieste.
 * Permette di gestire callback di successo, fallimento e timeout.
 *
 * @author Carlo Sandroni
 * @version 1.0, 12/12/2014
 */
public interface MessageReplyCallback<T extends GenericMessage> {
  /**
   * Metodo di callback su timeout della richiesta.
   *
   * @param message richiesta che ha originato il timeout
   * @param info  messaggio associato al timeout
   */
  default void onTimeout(T message, String info) {
    System.out.println("Timeout from " + message.getOntologyName() + ": " + info);
  }

  /**
   * Metodo di callback su successo della richiesta.
   *
   * @param message richiesta che ha originato il timeout
   * @param info  messaggio associato al successo
   */
  default void onSuccess(T message, String info) {

  }

  /**
   * Metodo di callback su risposta di tipo INFORM.
   *
   * @param message richiesta che ha originato il timeout
   * @param info  messaggio associato
   */
  default void onInform(T message, String info) {

  }

  /**
   * Metodo di callback su fallimento della richiesta.
   *
   * @param message richiesta che ha originato il timeout
   * @param info  messaggio associato al timeout
   */
  default void onFail(T message, String info) {
    System.out.println("Fail from " + message.getOntologyName() + ": " + info);
  }
}
