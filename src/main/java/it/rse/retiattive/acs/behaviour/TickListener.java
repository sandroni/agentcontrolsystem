package it.rse.retiattive.acs.behaviour;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.agent.TimedAgent;
import it.rse.retiattive.acs.domain.TimeInfo;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Behaviour per gli agenti di tipo TimedAgent che devono ricevere i segnali di clock.
 *
 * @author Carlo Sandroni
 * @version 1.0, 20/3/2014
 * @see TimedAgent
 */
public class TickListener extends Behaviour {
  private MapsOntologyHelper<TimeInfo> timeTopicHelper
      = new MapsOntologyHelper<>(TimeInfo.TOPIC_NAME);
  private MessageTemplate timeMt;
  private boolean error = false;
  TimedAgent myTimedAgent;


  /**
   * Default constructor.
   *
   * @param agent TimedAgent that owns the behaviour.
   */
  public TickListener(TimedAgent agent) {
    super((Agent) agent);

    try {
      if (agent == null) {
        throw new MapsException("Agente non valido.");
      }
      myTimedAgent = agent;
      timeMt = timeTopicHelper.createTopicTemplate(myAgent);
    } catch (MapsException e) {
      e.printStackTrace();
      error = true;
    }


  }

  @Override
  public void action() {
    if (!error) {
      ACLMessage msg = myAgent.receive(timeMt);
      if (msg != null) {
        try {
          TimeInfo time = timeTopicHelper.decodeMessage(msg, TimeInfo.class);
          myTimedAgent.onTick(time);
        } catch (MapsException e) {
          e.printStackTrace();
        }
      } else {
        block();
      }
    }
  }

  @Override
  public boolean done() {
    return error;
  }
}
