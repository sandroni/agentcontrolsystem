package it.rse.retiattive.acs.behaviour;

import it.rse.retiattive.acs.comm.GenericMessage;
import jade.core.AID;
import jade.lang.acl.ACLMessage;

/**
 * Callback per i GenericMessage in arrivo.
 *
 * @author Carlo Sandroni
 * @version 1.0, 29/7/2015
 * @see it.rse.retiattive.acs.comm.GenericMessage
 */
public interface MessageArrivalCallback<T extends GenericMessage> {
  /**
   * Metodo di callback su arrivo messaggio.
   *
   * @param message messaggio in arrivo decodificato
   * @param sender  AID del mittente
   * @param reply   reply preimpostata
   */
  default void onMessage(T message, AID sender, ACLMessage reply) {
    System.out.println("Message from " + sender.getLocalName() + ": " + message);
  }

}
