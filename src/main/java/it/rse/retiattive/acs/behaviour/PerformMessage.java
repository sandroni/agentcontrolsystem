package it.rse.retiattive.acs.behaviour;

/**
 * Behaviour per la gestione dell'invio di un messaggio generico.
 *
 * @author Carlo Sandroni
 * @version 2.0, 28/07/2015
 */

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.comm.GenericMessage;
import it.rse.retiattive.acs.ontology.MapsOntologyHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.Logger;

public class PerformMessage<T extends GenericMessage> extends Behaviour {
  /** Passi della prcedura di scrittura variabili. */
  public enum PerformMessageStep {
    SEND, WAIT_FOR_ANSWER, DONE_OK, DONE_FAIL, TIMEOUT
  }

  /** Logger utility. */
  private Logger logger = Logger.getJADELogger(PerformMessage.class.getName());
  /** Helper per i GenericMessage. */
  private final MapsOntologyHelper<T> messageHelper;
  /** Destinatario del messaggio. */
  private AID receiver;
  /** Messaggio da inviare. */
  private T message;
  /** Callback da chiamare. */
  private MessageReplyCallback<T> callback;
  /** Passo di esecuzione attuale. */
  private PerformMessageStep step;
  /** ID della conversazione attuale. */
  private String convId;
  /** Controllo di timeout attivo. */
  private boolean timeoutCheck = false;
  /** Durata timeout. */
  private long timeoutPeriod = 0;
  private long timeoutTime;


  /**
   * Costruttore behaviour di invio messaggi.
   *
   * @param sender   Agente che esegue la richiesta
   * @param receiver AID agente destinatario
   * @param message  Messaggio da inviare
   * @param callback Callback dell'agente che esegue la richiesta
   */
  public PerformMessage(final Agent sender,
                        final AID receiver,
                        final T message,
                        final MessageReplyCallback<T> callback) {
    super(sender);
    step = PerformMessageStep.SEND;
    this.receiver = receiver;
    this.message = message;
    this.callback = callback;
    this.messageHelper = new MapsOntologyHelper<>(message.getOntologyName());
  }

  /**
   * Costruttore behaviour di invio messaggi.
   *
   * @param sender        Agente che esegue la richiesta
   * @param receiver      AID agente destinatario
   * @param message       Messaggio da inviare
   * @param callback      Callback dell'agente che esegue la richiesta
   * @param timeoutPeriod Periodo di timeout, in millisecondi
   */
  public PerformMessage(final Agent sender,
                        final AID receiver,
                        final T message,
                        final MessageReplyCallback<T> callback,
                        final long timeoutPeriod) {
    this(sender, receiver, message, callback);
    if (timeoutPeriod > 0) {
      this.timeoutPeriod = timeoutPeriod;
      this.timeoutCheck = true;
    }
  }


  @Override
  public final void action() {
    if (step.equals(PerformMessageStep.SEND)) {
      // Preparazione e invio del messaggio
      ACLMessage aclMessage = new ACLMessage(ACLMessage.REQUEST);
      aclMessage.addReceiver(receiver);
      convId = "C" + hashCode() + "_" + System.currentTimeMillis();
      aclMessage.setConversationId(convId);
      try {
        messageHelper.encodeMessage(aclMessage, message);
        myAgent.send(aclMessage);
        step = PerformMessageStep.WAIT_FOR_ANSWER;
        if (timeoutCheck) {
          timeoutTime = System.currentTimeMillis() + timeoutPeriod;
        }
      } catch (MapsException e) {
        e.printStackTrace();
        step = PerformMessageStep.DONE_FAIL;
        callback.onFail(message, "impossible to send");
      }

    } else if (step.equals(PerformMessageStep.WAIT_FOR_ANSWER)) {
      // Attesa risposta
      if (System.currentTimeMillis() < timeoutTime) {
        // Tempo rispettato
        MessageTemplate mt = MessageTemplate.MatchConversationId(convId);
        ACLMessage reply = myAgent.receive(mt);
        if (reply != null) {
          T replyData = null;
          try {
            replyData = messageHelper.decodeMessage(reply, (Class<T>) message.getClass());
          } catch (MapsException e) {
            e.printStackTrace();
          }
          if (reply.getPerformative() == ACLMessage.CONFIRM) {
            step = PerformMessageStep.DONE_OK;
            callback.onSuccess(replyData, "success");
          } else if (reply.getPerformative() == ACLMessage.INFORM) {
            step = PerformMessageStep.DONE_OK;
            callback.onInform(replyData, "data");
          } else if (reply.getPerformative() == ACLMessage.FAILURE) {
            step = PerformMessageStep.DONE_FAIL;
            callback.onFail(replyData, "Answer from " + receiver.getLocalName() + ": FAILURE");
          }
        } else {
          if (timeoutCheck) {
            block(timeoutTime - System.currentTimeMillis());
          } else {
            block();
          }
        }
      } else if (timeoutCheck) {
        // Fuori tempo
        step = PerformMessageStep.TIMEOUT;
        callback.onTimeout(message, "Timeout attesa di risposta");
      } else {
        block();
      }
    }
  }

  @Override
  public boolean done() {
    return (step.equals(PerformMessageStep.DONE_OK)
        || step.equals(PerformMessageStep.DONE_FAIL)
        || step.equals(PerformMessageStep.TIMEOUT));
  }

}
