package it.rse.retiattive.acs.domain;

import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.util.leap.Iterator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Agent description for the front end.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/08/2015
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AgentInfo {
  private String name = "";
  private String className = "";
  private AgentStatus status = AgentStatus.IDLE;
  private List<Service> services = new ArrayList<>();

  public AgentInfo() {
  }

  public AgentInfo(DFAgentDescription jadeDescription) {
    Iterator serviceIter = jadeDescription.getAllServices();
    while (serviceIter.hasNext()) {
      ServiceDescription sd = (ServiceDescription) serviceIter.next();
      services.add(new Service(sd.getType(), sd.getName()));
    }
    this.name = jadeDescription.getName().getLocalName();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public AgentStatus getStatus() {
    return status;
  }

  public void setStatus(AgentStatus status) {
    this.status = status;
  }

  public void addService(Service service) {
    services.add(service);
  }

  public void addService(String type, String name) {
    services.add(new Service(type, name));
  }

  public List<Service> getServices() {
    return services;
  }


  @XmlRootElement
  public class Service {
    @XmlElement
    private String type;
    @XmlElement
    private String name;

    public Service(String type, String name) {
      this.type = type;
      this.name = name;
    }

    public String getType() {
      return type;
    }

    public String getName() {
      return name;
    }

    @Override
    public String toString() {
      return "Service{"
          + "type='" + type + '\''
          + ", name='" + name + '\''
          + '}';
    }
  }

  @Override
  public String toString() {
    return "AgentStatus{"
        + "name='" + name + '\''
        + ", className='" + className + '\''
        + ", status=" + status
        + ", services=" + services
        + '}';
  }
}
