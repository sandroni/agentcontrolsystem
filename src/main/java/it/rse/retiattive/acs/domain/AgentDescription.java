package it.rse.retiattive.acs.domain;

import it.rse.retiattive.acs.ontology.MapsOntology;
import jade.core.AgentContainer;
import jade.core.ContainerID;
import jade.domain.JADEAgentManagement.CreateAgent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Classe che rappresenta la descrizione di un agente.
 *
 * @author Carlo Sandroni
 * @version 2.0, 24/7/2015
 * @since 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "agent", namespace = "http://www.rse-web.it/xml/maps/agents")
public class AgentDescription implements MapsOntology {
  public static final String ONTOLOGY_NAME = "agent-description";

  private String name;
  @XmlElement(name = "class")
  private String agentClass;

  @XmlTransient
  private String owner;
  @XmlTransient
  private String container;

    /*@XmlElementWrapper(name = "parameters", namespace = "http://www.rse-web.it/xml/maps/agent")
    @XmlElement(name = "parameter", namespace = "http://www.rse-web.it/xml/maps/agent")
    private List<Attribute> parameters = new ArrayList<>();*/

  @XmlTransient
  private List<String> arguments;

  @XmlTransient
  private HcAgentStatus status;

    /*@XmlElementWrapper(name = "fieldVariableList")
    @XmlElement(name = "fieldVariable")*/
    /*@XmlTransient
    private List<FieldVariable> fieldVariableList = new ArrayList<>();*/

  private AgentDescription() {
  }

  /**
   * Default constructor of an agent description.
   *
   * @param name       Agent name in Jade platform
   * @param agentClass Agent class to be loaded
   */
  public AgentDescription(String name, String agentClass) {
    this();
    this.name = name;
    this.agentClass = agentClass;
    this.status = HcAgentStatus.INITIALIZING;
  }

    /*public List<Attribute> getParameters() {
        return parameters;
    }*/


  public HcAgentStatus getStatus() {
    return status;
  }

  public void setStatus(HcAgentStatus status) {
    this.status = status;
  }

    /*public List<FieldVariable> getFieldVariableList() {
        return fieldVariableList;
    }*/


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAgentClass() {
    return agentClass;
  }

  public void setAgentClass(String agentClass) {
    this.agentClass = agentClass;
  }


  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getContainer() {
    return container;
  }

  public void setContainer(String container) {
    this.container = container;
  }

  public List<String> getArguments() {
    return arguments;
  }

  public void setArguments(List<String> arguments) {
    this.arguments = arguments;
  }

  public void setArguments(String[] arguments) {
    this.arguments = Arrays.asList(arguments);
  }

  /**
   * Add an argument to the argument list.
   *
   * @param arg argument
   */
  public void addArgument(String arg) {
    if (arguments == null) {
      arguments = new ArrayList<>();
    }
    arguments.add(arg);
  }


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AgentDescription {");
    sb.append("\n\tname='").append(name).append('\'');
    sb.append("\n\tagentClass='").append(agentClass).append('\'');
    sb.append("\n\towner='").append(owner).append('\'');
    sb.append("\n\tcontainer='").append(container).append('\'');
    sb.append("\n\targuments=").append(arguments);
//        sb.append("\n\tparameters=").append(parameters);
//        sb.append("\n\tfieldVariables=").append(fieldVariableList);
    sb.append("\n}");
    return sb.toString();
  }

  /**
   * Build the CreateAgent object from the AgentDescription.
   *
   * @return CreateAgent object to be passed to the agent platform.
   */
  public CreateAgent getCreateAgent() {
    CreateAgent ca = new CreateAgent();
    // Fill name
    ca.setAgentName(name);

    // Fill class name
    ca.setClassName(agentClass);

    // Fill container with default container if not spcified
    if (container == null || "".equals(container)) {
      container = AgentContainer.MAIN_CONTAINER_NAME;
    }
    ca.setContainer(new ContainerID(container, null));

    ca.setOwner(null);
    ca.setInitialCredentials(null);

    if (arguments != null) {
      arguments.forEach(ca::addArguments);
    }

    return ca;
  }

  @Override
  public String getOntologyName() {
    return ONTOLOGY_NAME;
  }
}
