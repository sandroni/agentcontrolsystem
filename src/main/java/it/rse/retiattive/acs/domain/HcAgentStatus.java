package it.rse.retiattive.acs.domain;

/**
 * Status of Hierarchical Control Agent.
 *
 * @author Carlo Sandroni
 * @version 1.0, 23/03/2015
 */
public enum HcAgentStatus {
  INITIALIZING(0),
  READY(1),
  PAUSED(2),

  WAITING_FOR_UP_INPUT(10),
  UPWARD_WORKING(11),
  WAITING_FOR_UP_ACK(12),
  WAITING_FOR_DOWN_INPUT(13),
  DOWNWARD_WORKING(14),
  WAITING_FOR_DOWN_ACK(15),

  INIT_ERROR(101),
  ELABORATION_ERROR(102),
  COMM_TIMEOUT(201),
  ELABORATION_TIMEOUT(202);

  private int statusCode;

  HcAgentStatus(int statusCode) {
    this.statusCode = statusCode;
  }

  /**
   * Determine if the status is busy.
   *
   * @return true if the status is busy
   */
  public boolean isBusy() {
    return (statusCode >= 10 && statusCode < 100);
  }

  /**
   * Determine if the status is in error.
   *
   * @return true if the status is an error status
   */
  public boolean isError() {
    return statusCode >= 100;
  }

  /**
   * Determine if the status is in timeout error.
   *
   * @return true if the status is a timeout error
   */
  public boolean isTimeout() {
    return statusCode >= 200;
  }
}
