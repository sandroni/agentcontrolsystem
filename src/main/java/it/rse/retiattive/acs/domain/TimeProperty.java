package it.rse.retiattive.acs.domain;

/**
 * Property of a time instant.
 *
 * @author Carlo Sandroni
 * @version 1.0, 07/04/2015
 * @see TimeInfo
 */
public enum TimeProperty {
  FIRST_OF_TRACKING,
  FIRST_OF_ENERGY,
  FIRST_OF_PROGRAM,
}
