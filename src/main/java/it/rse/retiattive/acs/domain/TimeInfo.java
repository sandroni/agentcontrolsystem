package it.rse.retiattive.acs.domain;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.ontology.MapsOntology;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Informazioni temporali.
 *
 * @author Carlo Sandroni
 * @version 2.0, 27/7/2015
 * @since 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TimeInfo implements MapsOntology {
  public static final String TOPIC_NAME = "TimeInfo";
  private static final String configFilename = "maps.properties";
  public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");

  /** All times periods are in seconds. */
  private static int baseStep;        // 5s
  private static int programStep;     // 30m
  private static int energyStep;      // 15m
  private static int trackingStep;    // 60s

  private final Calendar time;

  private static int baseSlot;
  private static int trackingSlot;
  private static int energySlot;
  private static int programSlot;
  private static boolean needConfig = true;

  @XmlElementWrapper(name = "properties")
  @XmlElement(name = "property")
  private Set<TimeProperty> properties = new HashSet<>();
  private int secondOfDay;
  private int minuteOfDay;

  /**
   * Create a TimeInfo object using current date and time.
   */
  public TimeInfo() {
    this(Calendar.getInstance());
  }

  /**
   * Create a TimeInfo object using given date and time.
   * @param time Calendar object to be used to extract date and time info.
   */
  public TimeInfo(Calendar time) {
    if (needConfig) {
      try {
        config();
        needConfig = false;
      } catch (MapsException e) {
        e.printStackTrace();
      }
    }
    this.time = time;
    calcSlots();
  }

  private void config() throws MapsException {
    Properties prop = new Properties();
    try (InputStream input = getClass().getClassLoader().getResourceAsStream(configFilename)) {
      if (input != null) {
        prop.load(input);
        baseStep = Integer.valueOf(prop.getProperty("time.base")) / 1000;
        programStep = Integer.valueOf(prop.getProperty("time.programStep"));
        energyStep = Integer.valueOf(prop.getProperty("time.energyStep"));
        trackingStep = Integer.valueOf(prop.getProperty("time.trackingStep"));

      } else {
        throw new IOException("Config file not found");
      }
    } catch (IOException e) {
      throw new MapsException("MAPS Configuration error", e);
    }
  }

  private void calcSlots() {

    minuteOfDay = time.get(Calendar.HOUR_OF_DAY) * 60
        + time.get(Calendar.MINUTE);
    secondOfDay = minuteOfDay * 60
        + time.get(Calendar.SECOND);

    baseSlot = Math.floorDiv(secondOfDay, baseStep);
    trackingSlot = Math.floorDiv(Math.floorMod(secondOfDay, energyStep), trackingStep);
    energySlot = Math.floorDiv(secondOfDay, energyStep);
    programSlot = Math.floorDiv(secondOfDay, programStep);

    if (Math.floorMod(baseSlot, Math.floorDiv(trackingStep, baseStep)) == 0) {
      properties.add(TimeProperty.FIRST_OF_TRACKING);
    }
    if (Math.floorMod(baseSlot, Math.floorDiv(energyStep, baseStep)) == 0) {
      properties.add(TimeProperty.FIRST_OF_ENERGY);
    }
    if (Math.floorMod(baseSlot, Math.floorDiv(programStep, baseStep)) == 0) {
      properties.add(TimeProperty.FIRST_OF_PROGRAM);
    }
  }

  public Calendar getTime() {
    return time;
  }

  public String getTimeString() {
    return sdf.format(time.getTime());
  }

  public int getBaseSlot() {
    return baseSlot;
  }

  public int getTrackingSlot() {
    return trackingSlot;
  }

  public int getEnergySlot() {
    return energySlot;
  }

  public int getProgramSlot() {
    return programSlot;
  }

  public Set<TimeProperty> getProperties() {
    return properties;
  }

  public int getSecondOfDay() {
    return secondOfDay;
  }

  public int getMinuteOfDay() {
    return minuteOfDay;
  }

  public boolean isFirstOfTracking() {
    return properties.contains(TimeProperty.FIRST_OF_TRACKING);
  }

  public boolean isFirstOfEnergy() {
    return properties.contains(TimeProperty.FIRST_OF_ENERGY);
  }

  public boolean isFirstOfProgram() {
    return properties.contains(TimeProperty.FIRST_OF_PROGRAM);
  }

  @Override
  public String getOntologyName() {
    return TOPIC_NAME;
  }

  @Override
  public String toString() {
    return "TimeInfo: " + sdf.format(time.getTime()) + " (" + properties + ")";
  }
}
