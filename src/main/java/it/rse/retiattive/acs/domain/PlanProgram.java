package it.rse.retiattive.acs.domain;

import it.rse.util.MapsSerializable;
import it.rse.util.adapter.PlanDateAdapter;
import it.rse.util.adapter.PlanTimeAdapter;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Plan program binding class.
 *
 * @author Carlo Sandroni
 * @version 1.0, 09/09/2015
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "plans")
public class PlanProgram implements MapsSerializable {
  @XmlElement(name = "DailyProductionPlan")
  private List<DailyPlan> plans;

  public List<DailyPlan> getPlans() {
    return plans;
  }

  /**
   * Find the item nearest to the given datetime.
   * @param date Datetime to find
   * @return step nearest to given datetime
   */
  public DailyPlan findPlan(Date date) {
    Calendar dateToFind = Calendar.getInstance();
    dateToFind.setTime(date);
    for (DailyPlan plan : plans) {
      Calendar planDate = Calendar.getInstance();
      planDate.setTime(plan.getDay());
      if (planDate.get(Calendar.DAY_OF_MONTH) == dateToFind.get(Calendar.DAY_OF_MONTH)
          && planDate.get(Calendar.MONTH) == dateToFind.get(Calendar.MONTH)
          && planDate.get(Calendar.YEAR) == dateToFind.get(Calendar.YEAR)) {
        return plan;
      }
    }
    return null;
  }

  @Override
  public String toString() {
    return "PlanProgram{"
        + "plans=" + plans
        + '}' + '\n';
  }

  /** Called after object is unmarshalled. */
  public void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
    for (DailyPlan plan : plans) {
      Calendar day = Calendar.getInstance();
      day.setTime(plan.getDay());
      for (Unit unit : plan.getUnits()) {
        for (Attribute attribute : unit.getAttributes()) {
          for (Item item : attribute.getItems()) {
            Calendar datetime = Calendar.getInstance();
            datetime.setTime(item.getTimestamp());
            datetime.set(Calendar.DAY_OF_MONTH, day.get(Calendar.DAY_OF_MONTH));
            datetime.set(Calendar.MONTH, day.get(Calendar.MONTH));
            datetime.set(Calendar.YEAR, day.get(Calendar.YEAR));
            item.setTimestamp(datetime.getTime());
          }
        }
      }
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class DailyPlan {
    @XmlElement
    @XmlJavaTypeAdapter(PlanDateAdapter.class)
    private Date day;
    @XmlElement(name = "Unit")
    @XmlElementWrapper(name = "ControllableUnits")
    private List<Unit> ctrlUnits;
    @XmlElement(name = "Unit")
    @XmlElementWrapper(name = "NonControllableUnits")
    private List<Unit> noCtrlUnits;

    public Date getDay() {
      return day;
    }

    public List<Unit> getCtrlUnits() {
      return ctrlUnits;
    }

    public List<Unit> getNoCtrlUnits() {
      return noCtrlUnits;
    }

    public List<Unit> getUnits() {
      List<Unit> units = new ArrayList<>(ctrlUnits);
      units.addAll(noCtrlUnits);
      return units;
    }

    @Override
    public String toString() {
      return "DailyPlan{"
          + "day=" + day
          + ", ctrlUnits=" + ctrlUnits
          + ", noCtrlUnits=" + noCtrlUnits
          + '}' + '\n';
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class Unit {
    @XmlElement
    private String name;
    @XmlElement(name = "class")
    private String unitClass;
    @XmlElement(name = "attribute")
    private List<Attribute> attributes;

    public String getName() {
      return name;
    }

    public String getUnitClass() {
      return unitClass;
    }

    public List<Attribute> getAttributes() {
      return attributes;
    }

    @Override
    public String toString() {
      return "Unit{"
          + "name='" + name + '\''
          + ", unitClass='" + unitClass + '\''
          + ", attributes=" + attributes
          + '}' + '\n';
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class Attribute {
    @XmlElement
    private String name;
    @XmlElement(name = "item")
    @XmlElementWrapper(name = "program")
    private List<Item> items;
    @XmlTransient
    private Date dateToFind;

    public String getName() {
      return name;
    }

    public List<Item> getItems() {
      return items;
    }

    /**
     * Find the item nearest to the given datetime.
     * @param date Datetime to find
     * @return step nearest to given datetime
     */
    public Item findClosestStep(Date date) {
      dateToFind = date;
      return Collections.min(items,
          (Item step1, Item step2) -> {
          Date date1 = step1.getTimestamp();
          Date date2 = step2.getTimestamp();
          Date myDate = this.dateToFind;
          Long diff1m = Math.abs(myDate.getTime() - date1.getTime());
          Long diff2m = Math.abs(myDate.getTime() - date2.getTime());
          return diff1m.compareTo(diff2m);
        }
      );
    }

    @Override
    public String toString() {
      return "Attribute{"
          + "name='" + name + '\''
          + ", items=" + items
          + '}' + '\n';
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class Item {
    @XmlElement
    private int slot;
    @XmlElement(name = "referenceTime")
    @XmlJavaTypeAdapter(PlanTimeAdapter.class)
    private Date timestamp;
    @XmlElement(name = "setPoint")
    private double value;

    public int getSlot() {
      return slot;
    }

    public void setTimestamp(Date timestamp) {
      this.timestamp = timestamp;
    }

    public Date getTimestamp() {
      return timestamp;
    }

    public double getValue() {
      return value;
    }

    @Override
    public String toString() {
      return "Item{"
          + "slot=" + slot
          + ", timestamp=" + timestamp
          + ", value=" + value
          + '}' + '\n';
    }
  }
}
