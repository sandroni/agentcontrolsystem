package it.rse.retiattive.acs.domain;

/**
 * Status for a MAPS agent.
 *
 * @author Carlo Sandroni
 * @version 1.0, 07/09/2015
 */
public enum AgentStatus {
  IDLE,
  ENABLED,
  RUNNING,
  ERROR
}
