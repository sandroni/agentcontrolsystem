package it.rse.retiattive.acs.domain;

import it.rse.util.MapsSerializable;
import it.rse.util.adapter.LoadDateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Load program binding class.
 *
 * @author Carlo Sandroni
 * @version 1.0, 08/09/2015
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "loads")
public class LoadProgram implements MapsSerializable {
  @XmlElement(name = "node")
  private List<Node> nodes;

  public List<Node> getNodes() {
    return nodes;
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class Node {
    @XmlAttribute
    private int id;
    @XmlAttribute(name = "power_unit")
    private String powerUnit;
    @XmlElement(name = "step")
    private List<Step> steps;
    @XmlTransient
    private Date dateToFind;

    public List<Step> getSteps() {
      return steps;
    }

    public int getId() {
      return id;
    }

    /**
     * Find the step nearest to the given datetime.
     * @param date Datetime to find
     * @return step nearest to given datetime
     */
    public Step findClosestStep(Date date) {
      dateToFind = date;
      return Collections.min(steps,
          (Step step1, Step step2) -> {
          Date date1 = step1.getTime();
          Date date2 = step2.getTime();
          Date myDate = this.dateToFind;
          Long diff1m = Math.abs(myDate.getTime() - date1.getTime());
          Long diff2m = Math.abs(myDate.getTime() - date2.getTime());
          return diff1m.compareTo(diff2m);
        }
      );
    }

    @Override
    public String toString() {
      return "Node{"
          + "id=" + id
          + ", powerUnit='" + powerUnit + '\''
          + ", steps=" + steps
          + '}' + '\n';
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class Step {
    @XmlAttribute
    private int slot;
    @XmlAttribute
    private int elem;
    @XmlAttribute
    private double p;
    @XmlAttribute
    private double q;
    @XmlAttribute
    @XmlJavaTypeAdapter(LoadDateAdapter.class)
    private Date time;

    private Step() {
    }

    public Step(Date time) {
      this.time = time;
    }

    public Date getTime() {
      return time;
    }

    public double getP() {
      return p;
    }

    public double getQ() {
      return q;
    }

    public int getSlot() {
      return slot;
    }

    public int getElem() {
      return elem;
    }

    @Override
    public String toString() {
      return "Step{"
          + "slot=" + slot
          + ", elem=" + elem
          + ", p=" + p
          + ", q=" + q
          + ", time=" + time
          + '}' + '\n';
    }
  }

  @Override
  public String toString() {
    return "LoadProgram{"
        + "nodes=" + nodes
        + '}';
  }
}
