package it.rse.retiattive.acs.domain;

import it.rse.retiattive.acs.ontology.MapsOntology;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Calendar;

/**
 * State variable, suche as measurements.
 *
 * @author Carlo Sandroni
 * @version 1.0, 28/07/2015
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sv")
public class StateVariable implements MapsOntology {
  public static final String ONTOLOGY_NAME = "state-variable";

  @XmlAttribute(name = "name")
  private String name;
  @XmlAttribute(name = "value")
  private Double value = null;
  @XmlAttribute(name = "timestamp")
  private Calendar timestamp = null;

  private StateVariable() {
  }

  public StateVariable(String name) {
    this.name = name;
  }

  public StateVariable(String name, Double value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public Double getValue() {
    return value;
  }

  public StateVariable setValue(Double value) {
    this.value = value;
    return this;
  }

  public Calendar getTimestamp() {
    return timestamp;
  }

  public StateVariable setTimestamp(Calendar timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  @Override
  public String getOntologyName() {
    return ONTOLOGY_NAME;
  }
}
