package it.rse.retiattive.acs.domain;

import it.rse.util.MapsSerializable;
import jade.domain.JADEAgentManagement.CreateAgent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Lista di descrizioni di agenti.
 *
 * @author Carlo Sandroni
 * @version 2.0, 24/7/2015
 * @since 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "agents", namespace = "http://www.rse-web.it/xml/maps/agents")
public class AgentDescriptionList implements MapsSerializable {
  @XmlElement(name = "agent")
  List<AgentDescription> agents = new ArrayList<>();

  public List<AgentDescription> getAgents() {
    return agents;
  }

  public void setAgents(List<AgentDescription> agents) {
    this.agents = agents;
  }

  public void addAgent(AgentDescription agent) {
    this.agents.add(agent);
  }

  @Override
  public String toString() {
    return "AgentDescriptionList{" + "agents=" + agents + '}';
  }

  /**
   * Convert the list of AgentDescription in a list of CreateAgent objects, ready to be provided to the AMS for
   * the agent creation.
   *
   * @return List of CreateAgent objects
   */
  public List<CreateAgent> getCreateAgentList() {
    List<CreateAgent> caList = new ArrayList<>();
    for (AgentDescription agent : agents) {
      caList.add(agent.getCreateAgent());
    }
    return caList;
  }


  /**
   * Build a Map of AgentsDescription objects indexed by their names.
   *
   * @return map of AgendDescription objects
   */
  public Map<String, AgentDescription> getNamesMap() {
    if (agents.size() > 0) {
      Map<String, AgentDescription> agMap = new HashMap<>();
      for (AgentDescription agent : agents) {
        agMap.put(agent.getName(), agent);
      }
      return agMap;
    } else {
      return Collections.emptyMap();
    }
  }

  public int length() {
    return agents.size();
  }
}