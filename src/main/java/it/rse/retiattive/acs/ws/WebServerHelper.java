package it.rse.retiattive.acs.ws;

import it.rse.retiattive.acs.domain.AgentInfo;
import it.rse.retiattive.acs.domain.AgentStatus;
import it.rse.retiattive.acs.ws.rest.AgentsEntryPoint;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Webservice utility.
 *
 * @author Carlo Sandroni
 * @version 1.0, 01/09/2015
 */
public class WebServerHelper {

  private Server server;
  private Map<String, AgentInfo> mapsAgents;


  public WebServerHelper(Map<String, AgentInfo> mapsAgents) {
    this.mapsAgents = mapsAgents;
  }

  /**
   * Start a webservice.
   */
  public void start() {

    ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    context.setContextPath("/");

    ServletHolder staticServlet = context.addServlet(DefaultServlet.class, "/*");
    staticServlet.setInitParameter("resourceBase", "webroot");
    staticServlet.setInitParameter("pathInfoOnly", "true");

    ResourceConfig resourceConfig = new ResourceConfig();
    resourceConfig.register(new AgentsEntryPoint(mapsAgents));
    ServletContainer servletContainer = new ServletContainer(resourceConfig);
    ServletHolder jerseyServlet = new ServletHolder(servletContainer);
    context.addServlet(jerseyServlet, "/api/*");

    server = new Server(9090);
    server.setHandler(context);

    try {
      server.start();
//      server.join();
    } catch (Exception e) {
      e.printStackTrace();
    } /*finally {
      server.destroy();
    }*/
  }

  /**
   * Start a webservice.
   */
  public void startOld() {
    ResourceHandler resourceHandler = new ResourceHandler();
    resourceHandler.setDirectoriesListed(true);
    resourceHandler.setWelcomeFiles(new String[]{"index.html"});
    resourceHandler.setResourceBase("./webroot/");
    ContextHandler uiContext = new ContextHandler("/");
    uiContext.setHandler(resourceHandler);

    ServletContextHandler wsContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
    wsContext.setContextPath("/api");
    ServletHolder jerseyServlet = wsContext.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
    jerseyServlet.setInitOrder(0);
    /*jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
        FirstEntryPoint.class.getCanonicalName());*/
    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_PACKAGES, "it.rse.retiattive.acs.ws.rest");


    ResourceConfig rc = new ResourceConfig();
    Integer resource = 666;
    rc.register(resource);


//    wsContext.setAttribute("map", obj);

    ContextHandlerCollection contexts = new ContextHandlerCollection();
    contexts.setHandlers(new Handler[]{uiContext, wsContext});
    

    server = new Server(9090);
    server.setHandler(contexts);


    try {
      server.start();
      server.join();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      server.destroy();
    }
  }

  public void stop() {
    if (server != null) {
      try {
        server.stop();
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        server.destroy();
      }
    }
  }

  /**
   * Alternate start.
   */
  public void start2() {

    ServletContextHandler sch = new ServletContextHandler();
    sch.setContextPath("/xxx");

    ResourceConfig rc = new ResourceConfig();
    Integer resource = 666;
    rc.register(resource);

    ServletContainer sc = new ServletContainer(rc);
    ServletHolder holder = new ServletHolder(sc);
    sch.addServlet(holder, "/*");
    holder.setInitParameter(ServerProperties.PROVIDER_PACKAGES, "it.rse.retiattive.acs.ws.rest");

    server = new Server(9090);
    server.setHandler(sch);

    try {
      server.start();
      server.join();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      server.destroy();
    }
  }


  private static List<AgentInfo> getDefaultList() {
    List<AgentInfo> agentList = new ArrayList<>();
    AgentInfo agent1 = new AgentInfo();
    agent1.setName("MyDMS_2");
    agent1.setClassName("it.rse.dms");
    agent1.setStatus(AgentStatus.IDLE);
    AgentInfo agent2 = new AgentInfo();
    agent2.setName("MyET_2");
    agent2.setClassName("it.rse.et");
    agent2.setStatus(AgentStatus.IDLE);
    agentList.add(agent1);
    agentList.add(agent2);
    return agentList;
  }

}
