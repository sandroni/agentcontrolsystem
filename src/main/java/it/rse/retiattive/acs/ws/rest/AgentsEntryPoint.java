package it.rse.retiattive.acs.ws.rest;

import it.rse.retiattive.acs.domain.AgentInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


//import javax.ws.rs.core.Response;
//import javax.xml.ws.Response;

/**
 * TODO Add description.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/08/2015
 */
@Path("/agents")
public class AgentsEntryPoint {

  Map<String, AgentInfo> mapsAgents;
//  static int served = 0;


  public AgentsEntryPoint(Map<String, AgentInfo> mapsAgents) {
    this.mapsAgents = mapsAgents;
  }

  @GET
  @Path("test")
  @Produces(MediaType.TEXT_PLAIN)
  public String test() {
    return "test";
  }

  @GET
  @Path("test404")
  @Produces(MediaType.APPLICATION_JSON)
  public AgentInfo test404() {
    throw new WebApplicationException(Response
        .status(Response.Status.NOT_FOUND)
        .entity("Requested resource cannot be found.")
        .build());
  }

  /*@GET
  @Path("{id : \\d+}")
  @Produces(MediaType.APPLICATION_JSON)
  public AgentStatus getAgentById(@PathParam("id") String id) {
    if (mapsAgents.size() > Integer.valueOf(id)) {
      return mapsAgents.values().(Integer.valueOf(id));
    } else {
      throw new WebApplicationException(404);
    }
  }*/

  @GET
  @Path("{agentname : [a-zA-Z][a-zA-Z_0-9]*}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAgentByName(@PathParam("agentname") String agentName) {
    //return "Agent By Name: " + agentName;
    if (mapsAgents.containsKey(agentName)) {
//      return mapsAgents.get(agentName);
      return Response.status(Response.Status.OK).entity(mapsAgents.get(agentName)).build();
    } else {
      return Response.status(Response.Status.NOT_FOUND).entity("Can't find agent " + agentName).build();
//      throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
  }

  @GET
  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAgentList() {
    List<AgentInfo> agentList = new ArrayList<>(mapsAgents.values());
//    System.out.println("Servizio richiamato: " + agentList.toString());
//    served++;
//    if (Math.floorMod(served, 50) == 0)
//      System.out.println("Servizio richiamato " + served + " volte.");
    GenericEntity<List<AgentInfo>> entity = new GenericEntity<List<AgentInfo>>(agentList) {};
    return Response.ok(entity).build();
  }


}
