package it.rse.retiattive.acs.ws;

import it.rse.retiattive.acs.domain.AgentInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO Add description.
 *
 * @author Carlo Sandroni
 * @version 1.0, 02/09/2015
 */
public class WebApp {
  public static void main(String[] args) {

    Map<String, AgentInfo> map = new HashMap<>();

    AgentInfo ag1 = new AgentInfo();
    ag1.setName("Agent_1");
    ag1.addService("ServiceType_1", "ServiceName_1");
    ag1.addService("ServiceType_2", "ServiceName_2");
    map.put(ag1.getName(), ag1);

    AgentInfo ag2 = new AgentInfo();
    ag2.setName("Agent_2");
    ag2.addService("ServiceType_1", "ServiceName_21");
    ag2.addService("ServiceType_3", "ServiceName_3");
    map.put(ag2.getName(), ag2);

    WebServerHelper ws = new WebServerHelper(map);
    ws.start();
  }

}
