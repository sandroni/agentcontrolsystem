package it.rse.retiattive.acs.ontology;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.comm.MessageLanguage;
import it.rse.util.XmlHelper;
import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBException;

/**
 * Classe helper per definire i template, codificare e decodificare i messaggi, sottoscrivere i topic.
 *
 * @author Carlo Sandroni
 * @version 1.0, 21/11/2014
 */
public class MapsOntologyHelper<T extends MapsOntology> {
  private int templatePerformative = ACLMessage.REQUEST;
  private String language = MessageLanguage.XML;
  private String ontologyName;
  AID topic = null;

  public MapsOntologyHelper(T ontology) {
    this.ontologyName = ontology.getOntologyName();
  }

  public MapsOntologyHelper(String ontologyName) {
    this.ontologyName = ontologyName;
  }

  public int getTemplatePerformative() {
    return templatePerformative;
  }

  public void setTemplatePerformative(int templatePerformative) {
    this.templatePerformative = templatePerformative;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public MessageTemplate createMessageTemplate() {
    return createMessageTemplate(null, language);
  }

  public MessageTemplate createMessageTemplate(int performative) {
    return createMessageTemplate(performative, language);
  }

  public MessageTemplate createMessageTemplate(String language) {
    return createMessageTemplate(null, language);
  }

  /**
   * Creates a MessageTemplate for the current ontology.
   * Suitable for filtering incoming messages.
   *
   * @param performative Type of message
   * @param language     Language of message
   * @return MessageTemplate for filtering current ontology
   */
  public MessageTemplate createMessageTemplate(Integer performative, String language) {
    MessageTemplate mt = MessageTemplate.and(
        MessageTemplate.MatchOntology(ontologyName),
        MessageTemplate.MatchLanguage(language)
    );
    if (performative != null) {
      return MessageTemplate.and(
          MessageTemplate.MatchPerformative(performative),
          mt
      );
    } else {
      return mt;
    }
    /*
    return MessageTemplate.and(
            MessageTemplate.MatchPerformative(performative),
            MessageTemplate.and(
                    MessageTemplate.MatchOntology(ontologyName),
                    MessageTemplate.MatchLanguage(language)
            ));
            */
  }

  /**
   * Decode a message according to the current ontology.
   *
   * @param msg   Message to be decoded
   * @param clazz Ontology class to be used for decoding
   * @return Object of the ontology class representing the message content
   * @throws MapsException if the deconding fails
   */
  public T decodeMessage(ACLMessage msg, Class<T> clazz) throws MapsException {
    if (msg == null || msg.getContent() == null) {
      return null;
    }
    try {
      if (MessageLanguage.XML.equals(msg.getLanguage())) {
        return XmlHelper.fromXml(msg.getContent(), clazz);
      } else {
        throw new MapsException("Language not supported: " + msg.getLanguage());
      }
    } catch (JAXBException e) {
      throw new MapsException("Deserialization error.", e);
    }
  }

  /**
   * Encode a message according to the current ontology.
   *
   * @param msg    Message to be filled
   * @param object Object to be encoded in the message
   * @throws MapsException if the encoding fails
   */
  public void encodeMessage(ACLMessage msg, T object) throws MapsException {
    msg.setLanguage(language);
    msg.setOntology(ontologyName);
    try {
      msg.setContent(object.serialize(language));
    } catch (JAXBException e) {
      throw new MapsException("Serialization error.", e);
    }
  }

  /**
   * Creates a topic for the current ontology where publishing messages.
   *
   * @param myAgent agent creating the topic
   * @return AID representing the queue where to send messages
   * @throws MapsException if the topic creation fails
   */
  public AID createTopic(Agent myAgent) throws MapsException {
    try {
      TopicManagementHelper topicHelper = (TopicManagementHelper) myAgent.getHelper(TopicManagementHelper.SERVICE_NAME);
      topic = topicHelper.createTopic(ontologyName);
      return topic;
    } catch (ServiceException e) {
      throw new MapsException("Impossibile creare Topic " + ontologyName, e);
    }
  }


  /**
   * Creates a MessageTemplate for the current ontology.
   * Filter this template for topic subscription.
   *
   * @param myAgent agent creating the template
   * @return MessageTempalte for topic messages filtering
   * @throws MapsException if the creation fails
   */
  public MessageTemplate createTopicTemplate(Agent myAgent) throws MapsException {
    try {
      TopicManagementHelper topicHelper = (TopicManagementHelper) myAgent.getHelper(TopicManagementHelper.SERVICE_NAME);
      AID timeTopic = topicHelper.createTopic(ontologyName);
      topicHelper.register(timeTopic);
      return MessageTemplate.MatchTopic(timeTopic);
    } catch (ServiceException e) {
      throw new MapsException("Impossibile sottoscrivere Topic " + ontologyName, e);
    }
  }

  /**
   * Encode a ontology object in a topic message ready to be published.
   * It uses the last topic created with createTopic.
   *
   * @param msg    message to be filled
   * @param object object to be encoded
   * @throws MapsException if the encoding fails
   */
  public void encodeTopicMessage(ACLMessage msg, T object) throws MapsException {
    encodeTopicMessage(msg, object, topic);
  }

  /**
   * Encode a ontology object in a topic message ready to be published.
   * It uses the given topic queue.
   *
   * @param msg    message to be filled
   * @param object object to be encoded
   * @param topic  topic where publishing the message
   * @throws MapsException if the encoding fails
   */
  public void encodeTopicMessage(ACLMessage msg, T object, AID topic) throws MapsException {
    if (topic == null) {
      throw new MapsException("Topic non inizializzato");
    }
    msg.addReceiver(topic);
    msg.setLanguage(language);
    msg.setOntology(ontologyName);
    try {
      msg.setContent(object.serialize(language));
    } catch (JAXBException e) {
      throw new MapsException("Serialization error.", e);
    }
  }

}