package it.rse.retiattive.acs.ontology;

import it.rse.util.MapsSerializable;

/**
 * Interfaccia da implementare nelle classi che devono essere scambiate come messaggi.
 *
 * @author Carlo Sandroni
 * @version 1.0, 21/11/2014
 */
public interface MapsOntology extends MapsSerializable {
  /**
   * Return the name of the ontology of the class.
   * THe name should be unique in the project.
   *
   * @return String with the ontology name
   */
  String getOntologyName();
}
