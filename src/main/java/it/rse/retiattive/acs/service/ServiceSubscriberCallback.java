package it.rse.retiattive.acs.service;

import jade.domain.FIPAAgentManagement.DFAgentDescription;

/**
 * Interfaccia per agenti che sottoscrivono servizi.
 *
 * @author Carlo Sandroni
 * @version 2.0, 28/07/2015
 */
public interface ServiceSubscriberCallback {
  void onServiceFound(DFAgentDescription[] serviceProviders);
}
