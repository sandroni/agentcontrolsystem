package it.rse.retiattive.acs.service;

import it.rse.retiattive.acs.MapsException;
import it.rse.retiattive.acs.domain.TimeInfo;
import jade.core.Agent;

/**
 * Time Service.
 *
 * @author Carlo Sandroni
 * @version 1.0, 31/08/2015
 */
public class TimeService implements AcsService {
  public static final String SERVICE_TYPE = "time-service";
  private String serviceName;

  /**
   * Default constructor with no name service.
   */
  private TimeService() {
    this("");
  }

  /**
   * Constructor for named services.
   * @param serviceName name of the service.
   */
  public TimeService(String serviceName) {
    this.serviceName = serviceName;
  }

  /**
   * Service registration.
   * @deprecated use ServiceHelper.registerServices(...) instead.
   *
   * @param agent       agente che registra il servizio
   * @param serviceName nome del servizio da registrare
   */
  @Deprecated
  @Override
  public void registerService(Agent agent, String serviceName) throws MapsException {
    ServicesHelper.registerService(agent, SERVICE_TYPE, serviceName);
  }

  /**
   * Get the service type identifier.
   *
   * @return service type
   */
  @Override
  public String getServiceType() {
    return SERVICE_TYPE;
  }

  /**
   * Get the service name identifier.
   *
   * @return service name
   */
  @Override
  public String getServiceName() {
    return serviceName;
  }
}
