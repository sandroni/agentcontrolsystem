package it.rse.retiattive.acs.service;

import it.rse.retiattive.acs.MapsException;
import jade.core.Agent;

/**
 * Monitor Service.
 *
 * @author Carlo Sandroni
 * @version 2.0, 28/07/2015
 * @since 1.0
 */
public class MonitorService implements AcsService {
  public static final String SERVICE_TYPE = "monitor-service";
  private String serviceName;

  /**
   * Default constructor with no name service.
   */
  private MonitorService() {
    this("");
  }

  /**
   * Constructor for named services.
   * @param serviceName name of the service.
   */
  public MonitorService(String serviceName) {
    this.serviceName = serviceName;
  }

  /**
   * Service registration.
   * @deprecated use ServiceHelper.registerServices(...) instead.
   *
   * @param agent       agente che registra il servizio
   * @param serviceName nome del servizio da registrare
   */
  @Deprecated
  @Override
  public void registerService(Agent agent, String serviceName) throws MapsException {
    ServicesHelper.registerService(agent, SERVICE_TYPE, serviceName);
  }

  /**
   * Get the service type identifier.
   *
   * @return service type
   */
  @Override
  public String getServiceType() {
    return SERVICE_TYPE;
  }

  /**
   * Get the service name identifier.
   *
   * @return service name
   */
  @Override
  public String getServiceName() {
    return serviceName;
  }
}
