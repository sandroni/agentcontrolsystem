package it.rse.retiattive.acs.service;

import it.rse.retiattive.acs.MapsException;
import jade.core.Agent;

/**
 * Interface for ACS Services.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/03/2015
 */
public interface AcsService {
  /**
   * Service registration.
   * @deprecated use ServiceHelper.registerServices(...) instead.
   *
   * @param agent       Agente che registra il servizio
   * @param serviceName nome del servizio da registrare
   */
  @Deprecated
  void registerService(Agent agent, String serviceName) throws MapsException;

  /**
   * Get the service type identifier.
   *
   * @return service type
   */
  String getServiceType();

  /**
   * Get the service name identifier.
   *
   * @return service name
   */
  String getServiceName();
}
