package it.rse.retiattive.acs.service;

import it.rse.retiattive.acs.MapsException;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionInitiator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Helper per la registrazione e sottoscrizione di servizi MAPS.
 *
 * @author Carlo Sandroni
 * @version 2.0, 28/7/2015
 * @since 1.0
 */
public class ServicesHelper {
  private List<AcsService> registeredServices;

  /**
   * Aggiunge un servizio alla lista dei servizi da registrare.
   * Successivamente chiamare registerService.
   *
   * @param service Servizio da aggiungere alla lista.
   */
  public void addService(AcsService service) {
    if (registeredServices == null) {
      registeredServices = new ArrayList<>();
    }
    registeredServices.add(service);
  }

  /**
   * Registra i servizi precedentemente aggiunti mediante addService().
   *
   * @param registrant Agente sul quale registrare i servizi.
   * @throws MapsException in caso di fallimento registrazione
   */
  public void registerServices(Agent registrant) throws MapsException {
    registerServices(registrant, (AcsService[]) registeredServices.toArray());
  }

  /**
   * Registra i servizi specificati.
   *
   * @param registrant Agente sul quale registrare i servizi.
   * @param services Array of services
   * @throws MapsException in caso di fallimento registrazione
   */
  public static void registerServices(Agent registrant, AcsService... services) throws MapsException {
    if (registrant == null) {
      throw new MapsException("Invalid register arguments");
    }
    if (services == null || services.length == 0) {
      throw new MapsException("No services to be registered");
    }
    DFAgentDescription dfd = new DFAgentDescription();
    dfd.setName(registrant.getAID());
    for (AcsService service : services) {
      ServiceDescription sd = new ServiceDescription();
      sd.setType(service.getServiceType());
      sd.setName(service.getServiceName());
      dfd.addServices(sd);
    }
    try {
      DFService.register(registrant, dfd);
    } catch (FIPAException e) {
      throw new MapsException("Impossibile registrare i servizi per l'agente " + registrant.getLocalName(), e);
    }
  }

  /**
   * Registra l'agente desiderato al servizio con con tipo e nome richiesto.
   * @deprecated use registerServices(...) instead.
   *
   * @param registrant  Agente da registrare come servizio
   * @param serviceType Tipo del servizio
   * @param serviceName Nome del servizio
   * @throws MapsException Eccezione lanciata in caso di registrazione non riuscita
   */
  @Deprecated
  public static void registerService(Agent registrant,
                                     String serviceType,
                                     String serviceName) throws MapsException {
    if (registrant == null
        || serviceType == null || serviceType.isEmpty()
        || serviceName == null || serviceName.isEmpty()) {
      throw new MapsException("Invalid register arguments");
    }
    DFAgentDescription dfd = new DFAgentDescription();
    dfd.setName(registrant.getAID());
    ServiceDescription sd = new ServiceDescription();
    sd.setType(serviceType);
    sd.setName(serviceName);
    dfd.addServices(sd);
    try {
      DFService.register(registrant, dfd);
    } catch (FIPAException e) {
      throw new MapsException("Impossibile registrare il servizio "
          + serviceType + ":" + serviceName
          + " per l'agente " + registrant.getLocalName(), e);
    }
  }

  /**
   * Ricerca degli agenti che espongono il servizio desiderato.
   *
   * @param finder      Agente che performa la ricerca
   * @param serviceType Tipo del servizio (nullable)
   * @param serviceName Nome del servizio (nullable)
   * @return Lista degli agenti che espongono il servizio desiderato
   * @throws MapsException if no service is found
   */
  public static List<AID> lookupService(@NotNull Agent finder,
                                        @NotNull String serviceType,
                                        @Nullable String serviceName) throws MapsException {
    DFAgentDescription template = new DFAgentDescription();
    ServiceDescription sd = new ServiceDescription();
    sd.setType(serviceType);
    if (serviceName != null) {
      sd.setName(serviceName);
    }
    template.addServices(sd);
    try {
      DFAgentDescription[] result = DFService.search(finder, template);
      if (result.length > 0) {
        List<AID> resultList = new ArrayList<>();
        for (DFAgentDescription ad : result) {
          resultList.add(ad.getName());
        }
        return resultList;
      } else {
        return Collections.emptyList();
      }
    } catch (FIPAException e) {
      throw new MapsException("Impossibile trovare il servizio richiesto", e);
    }
  }


  /**
   * Ricerca degli agenti che espongono il servizio desiderato.
   *
   * @param finder      Agente che performa la ricerca
   * @param serviceType Tipo del servizio
   * @return Lista degli agenti che espongono il servizio desiderato
   * @throws MapsException if no service is found
   */
  public static List<AID> lookupService(Agent finder, String serviceType) throws MapsException {
    return lookupService(finder, serviceType, null);
  }


  /**
   * Ricerca di un agente che espone il servizio desiderato.
   *
   * @param finder      Agente che performa la ricerca
   * @param serviceType Tipo del servizio (nullable)
   * @param serviceName Nome del servizio (nullable)
   * @return Primo agente che esponge il servizio desiderato
   * @throws MapsException if no service is found
   */
  @Nullable
  public static AID lookupFirstService(Agent finder,
                                       String serviceType,
                                       @Nullable String serviceName) throws MapsException {
    if (finder == null) {
      throw new MapsException("Invalid finder agent");
    }
    if (serviceType == null) {
      throw new MapsException("Invalid service type");
    }
    List<AID> serviceList = lookupService(finder, serviceType, serviceName);
    if (serviceList.isEmpty()) {
      return null;
    } else {
      return serviceList.iterator().next();
    }
  }

  /**
   * Ricerca di un agente che espone il servizio desiderato.
   *
   * @param finder      Agente che performa la ricerca
   * @param serviceType Tipo del servizio (nullable)
   * @return Primo agente che esponge il servizio desiderato
   * @throws MapsException if no service is found
   */
  @Nullable
  public static AID lookupFirstService(Agent finder, String serviceType) throws MapsException {
    return lookupFirstService(finder, serviceType, null);
  }


  /**
   * Subscribe for a specified service.
   *
   * @param finder      ServiceSubscriber agent
   * @param serviceType type of the service
   * @param serviceName name of the service
   * @param properties  list of properties requested
   */
  public static void subscribeService(@NotNull ServiceSubscriberCallback finder,
                                      @Nullable String serviceType,
                                      @Nullable String serviceName,
                                      @Nullable List<Property> properties) {
    ServiceDescription templateSd = new ServiceDescription();
    if (serviceType != null) {
      templateSd.setType(serviceType);
    }
    if (serviceName != null) {
      templateSd.setName(serviceName);
    }
    if (properties != null && properties.size() > 0) {
      properties.forEach(templateSd::addProperties);
    }
    DFAgentDescription template = new DFAgentDescription();
    template.addServices(templateSd);

    SearchConstraints sc = new SearchConstraints();
    // Get unlimited number of results:
    sc.setMaxResults(-1L);

    Agent myAgent = (Agent) finder;
    myAgent.addBehaviour(new SubscriptionInitiator(myAgent,
        DFService.createSubscriptionMessage(myAgent, myAgent.getDefaultDF(), template, sc)) {

      @Override
      protected void handleInform(ACLMessage inform) {
        try {
          DFAgentDescription[] results = DFService.decodeNotification(inform.getContent());
          if (results.length > 0) {
            finder.onServiceFound(results);
          }
        } catch (FIPAException fe) {
          fe.printStackTrace();
        }
      }
    });
  }

  /**
   * Subscribe for a specified service.
   *  @param finder      Finder agent
   * @param serviceType type of the service
   * @param serviceName name of the service
   * @param properties  list of properties requested
   * @param callback    Callback object
   */
  public static void subscribeService(@NotNull Agent finder,
                                      @Nullable String serviceType,
                                      @Nullable String serviceName,
                                      @Nullable List<Property> properties,
                                      @NotNull ServiceSubscriberCallback callback) {
    ServiceDescription templateSd = new ServiceDescription();
    if (serviceType != null) {
      templateSd.setType(serviceType);
    }
    if (serviceName != null) {
      templateSd.setName(serviceName);
    }
    if (properties != null && properties.size() > 0) {
      for (Property property : properties) {
        templateSd.addProperties(property);
      }
    }
    DFAgentDescription template = new DFAgentDescription();
    template.addServices(templateSd);

    SearchConstraints sc = new SearchConstraints();
    // Get unlimited number of results:
    sc.setMaxResults(-1L);

    finder.addBehaviour(new SubscriptionInitiator(finder,
        DFService.createSubscriptionMessage(finder, finder.getDefaultDF(), template, sc)) {

      @Override
      protected void handleInform(ACLMessage inform) {
        try {
          DFAgentDescription[] results = DFService.decodeNotification(inform.getContent());
          callback.onServiceFound(results);
        } catch (FIPAException fe) {
          fe.printStackTrace();
        }
      }
    });
  }

  /**
   * Deregister an agent.
   * Wrapper for DFService.deregister(Agent).
   *
   * @param agent Agent to be deregistered.
   * @throws MapsException if deregistration fails.
   */
  public static void deregister(Agent agent) throws MapsException {
    try {
      DFService.deregister(agent);
    } catch (FIPAException e) {
      throw new MapsException("Impossibile deregistrare agente " + agent.getLocalName());
    }
  }
}