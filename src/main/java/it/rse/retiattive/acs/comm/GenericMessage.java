package it.rse.retiattive.acs.comm;

import it.rse.retiattive.acs.ontology.MapsOntology;

/**
 * Interfaccia da implementare nelle classi che rappresentano messaggi a cui rispondere.
 *
 * @author Carlo Sandroni
 * @version 1.0, 28/07/2015
 */
public interface GenericMessage extends MapsOntology {

}
