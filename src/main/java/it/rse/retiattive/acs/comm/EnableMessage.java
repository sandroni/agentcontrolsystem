package it.rse.retiattive.acs.comm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Remote Enable Message.
 *
 * @author Carlo Sandroni
 * @version 2.0, 27/07/2015
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EnableMessage implements GenericMessage {
  public static final String ONTOLOGY_NAME = "EnableMessage";

  public enum Action {
    ENABLE,
    DISABLE
  }

  private Action action;

  private EnableMessage() {
  }

  public EnableMessage(Action action) {
    this.action = action;
  }

  @Override
  public String getOntologyName() {
    return ONTOLOGY_NAME;
  }

  public Action getAction() {
    return action;
  }

  public void setAction(Action action) {
    this.action = action;
  }


  @Override
  public String toString() {
    return "EnableMessage{"
        + "action=" + action
        + '}';
  }
}
