package it.rse.retiattive.acs.comm;

/**
 * Languages allowed.
 *
 * @author Carlo Sandroni
 * @version 2.0, 14/11/2014
 */
public class MessageLanguage {
  public static final String XML = "application/xml";
  public static final String XML_LZ4 = "application/lz4-xml";
}
