package it.rse.retiattive.acs.comm;

import it.rse.retiattive.acs.domain.StateVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Remote Enable Message.
 *
 * @author Carlo Sandroni
 * @version 2.0, 27/07/2015
 */
@XmlRootElement(name = "dataMessage")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataMessage implements GenericMessage {
  public static final String ONTOLOGY_NAME = "DataMessage";

  public enum Action {
    GET,
    SET,
    TELL,
  }

  @XmlElement
  private Action action;

  @XmlElementWrapper(name = "data")
  @XmlElement(name = "sv")
  private List<StateVariable> varList = new ArrayList<>();

  @XmlTransient
  private Map<String, StateVariable> varMap = new HashMap<>();

  public DataMessage() {
  }

  public DataMessage(Action action) {
    this.action = action;
  }


  public Action getAction() {
    return action;
  }

  public void setAction(Action action) {
    this.action = action;
  }

  public List<StateVariable> getVarList() {
    return varList;
  }

  /**
   * Set the entire list of variables.
   *
   * @param varList list of StateVariable
   * @return self DataMessage object
   */
  public DataMessage setVarList(List<StateVariable> varList) {
    this.varList = varList;
    afterUnmarshal(null, null);
    return this;
  }

  /**
   * Add a variable to the list.
   *
   * @param variable variable to add
   * @return self DataMessage object
   */
  public DataMessage addVariable(StateVariable variable) {
    this.varList.add(variable);
    afterUnmarshal(null, null);
    return this;
  }

  /**
   * Retrieve a variable with a given name.
   *
   * @param name name of the requested state variable
   * @return state variable requested
   */
  public StateVariable getVariable(String name) {
    return varMap.get(name);
  }

  public Map<String, StateVariable> getVarMap() {
    return varMap;
  }

  /** Called after object is unmarshalled. */
  public void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
    varMap.clear();
    for (StateVariable sv : varList) {
      varMap.put(sv.getName(), sv);
    }
  }

  @Override
  public String getOntologyName() {
    return ONTOLOGY_NAME;
  }

  @Override
  public String toString() {
    String str = "DataMessage";
    str = str + " (" + varList.size() + ") {";
    for (StateVariable sv : varList) {
      str = str + sv.getName() + "=" + sv.getValue() + "; ";
    }
    str = str + "}";
    return str;
  }
}
