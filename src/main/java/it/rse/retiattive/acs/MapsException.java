package it.rse.retiattive.acs;

/**
 * Eccezione generica di MAPS.
 *
 * @author Carlo Sandroni
 * @version 1.0, 20/11/2014
 */
public class MapsException extends Exception {
  public MapsException() {
  }

  public MapsException(String message) {
    super(message);
  }

  public MapsException(String message, Throwable cause) {
    super(message, cause);
  }

  public MapsException(Throwable cause) {
    super(cause);
  }

  public MapsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
