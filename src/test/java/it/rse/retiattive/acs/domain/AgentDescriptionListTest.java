package it.rse.retiattive.acs.domain;

import it.rse.util.XmlHelper;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;


/**
 * Test dell'importazione xml lista agenti.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/07/2015
 */
public class AgentDescriptionListTest {
    public static final String agentFileName = "platforms/etAgents.xml";
    private static Logger logger = Logger.getLogger(AgentDescriptionListTest.class.getName());
    private File file;

    @Before
    public void setUp() throws Exception {
        URL agentListUrl = getClass().getClassLoader().getResource(agentFileName);
        if (agentListUrl != null) {
            file = new File(agentListUrl.getFile());
            logger.info("File " + agentFileName + " caricato.");
        }
    }

    @Test
    public void testImportXml() throws Exception {
        AgentDescriptionList agList;
        if (file != null) {
            try {
                agList = XmlHelper.fromXml(file, AgentDescriptionList.class);
                System.out.println(agList.toString());
                assertThat(agList.getAgents(), hasSize(5));
            } catch (JAXBException e) {
                logger.severe("Formato file non supportato.");
                e.printStackTrace();
            }

        }
    }
}