package it.rse.retiattive.acs.domain;

import it.rse.util.XmlHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Test of the load file loader.
 *
 * @author Carlo Sandroni
 * @version 1.0, 08/09/2015
 */
public class LoadProgramTest {
  public static final String agentFileName = "./input/loads.xml";
  private static Logger logger = Logger.getLogger(LoadProgramTest.class.getName());
  private File file;

  @Before
  public void setUp() throws Exception {
    /*URL agentListUrl = getClass().getClassLoader().getResource(planFileName);
    if (agentListUrl != null) {
      file = new File(agentListUrl.getFile());
      logger.info("File " + planFileName + " caricato.");
    }*/
    file = new File(agentFileName);
    logger.info("File " + agentFileName + " caricato.");
  }

  @Test
  public void testImportXml() throws Exception {
    LoadProgram load;
    if (file != null) {
      try {
        long startTime = System.nanoTime();
        load = XmlHelper.fromXml(file, LoadProgram.class);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000;
        System.out.println("Unmashalling took " + duration + "ms");
//        System.out.println(load.toString());
        for (LoadProgram.Node node : load.getNodes()) {
          // Find <step slot="10" elem="2" day="16/01/2015" time="2015-01-16 02:20:00" p="27.725" q="0.000"/>
          Date dateToFind = new GregorianCalendar(2015, 0, 16, 2, 20, 0).getTime();
          System.out.println("Target: " + dateToFind.toString());
          startTime = System.nanoTime();
          LoadProgram.Step nearest = node.findClosestStep(dateToFind);
          endTime = System.nanoTime();
          duration = (endTime - startTime) / 1000000;
          System.out.println("Step piu' vicino: " + nearest.toString());
          System.out.println("Finding took " + duration + "ms");
          assertThat(nearest.getElem(), is(2));
          assertThat(nearest.getSlot(), is(10));
        }
      } catch (JAXBException e) {
        logger.severe("Formato file non supportato.");
        e.printStackTrace();
      }
    }
  }

  @After
  public void tearDown() throws Exception {


  }
}