package it.rse.retiattive.acs.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;


/**
 * TimeInfo test su singolo caso.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/07/2015
 */
public class TimeInfoTest {
    private TimeInfo timeInfo;

    @Before
    public void setUp() throws Exception {
        Calendar time = Calendar.getInstance();
        time.set(2015, Calendar.JULY, 27, 10, 0, 6);
        timeInfo = new TimeInfo(time);
    }

    @Test
    public void testTimeInfo() {
        System.out.println(timeInfo);
    }
}