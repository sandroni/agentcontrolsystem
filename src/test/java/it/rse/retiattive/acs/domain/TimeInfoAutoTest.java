package it.rse.retiattive.acs.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * TimeInfo test automatizzato.
 *
 * @author Carlo Sandroni
 * @version 1.0, 27/07/2015
 */
@RunWith(Parameterized.class)
public class TimeInfoAutoTest {
    private TimeInfo timeInfo;
    private String timeString;
    private int baseSlot, trackingSlot, energySlot, programSlot, secondOfDay, minuteOfDay;
    private boolean firstOfTracking, firstOfEnergy, firstOfProgram;
    private final Calendar time;

    public TimeInfoAutoTest(int hourOfDay,
                            int minute,
                            int second,
                            String timeString,
                            int minuteOfDay,
                            int secondOfDay,
                            int baseSlot,
                            int trackingSlot,
                            int energySlot,
                            int programSlot,
                            boolean firstOfTracking,
                            boolean firstOfEnergy,
                            boolean firstOfProgram) {
        this.timeString = timeString;
        this.baseSlot = baseSlot;
        this.trackingSlot = trackingSlot;
        this.energySlot = energySlot;
        this.programSlot = programSlot;
        this.secondOfDay = secondOfDay;
        this.minuteOfDay = minuteOfDay;
        this.firstOfTracking = firstOfTracking;
        this.firstOfEnergy = firstOfEnergy;
        this.firstOfProgram = firstOfProgram;

        time = Calendar.getInstance();
        time.set(Calendar.HOUR_OF_DAY, hourOfDay);
        time.set(Calendar.MINUTE, minute);
        time.set(Calendar.SECOND, second);
        timeInfo = new TimeInfo(time);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
//              hh, mm, ss,   str,     minDay,   secDay,       slot, track, en, prog, firstT, firstE, firstP
                {10, 0, 0, "10:00:00", 10*60+0, (10*60+0)*60+0, 7200, 0, 10*4, 10*2, true, true, true},
                {10, 0, 6, "10:00:06", 10*60+0, (10*60+0)*60+6, 7201, 0, 10*4, 10*2, false, false, false},
                {10, 5, 0, "10:05:00", 10*60+5, (10*60+5)*60+0, 7260, 5, 10*4, 10*2, true, false, false},
        });
    }

    @Test
    public void testGetTime() throws Exception {
        assertThat(timeInfo.getTime(), is(time));
    }

    @Test
    public void testGetTimeString() throws Exception {
        assertThat(timeInfo.getTimeString(), containsString(timeString));
    }

    @Test
    public void testGetBaseSlot() throws Exception {
        assertThat(timeInfo.getBaseSlot(), is(baseSlot));
    }

    @Test
    public void testGetTrackingSlot() throws Exception {
        assertThat(timeInfo.getTrackingSlot(), is(trackingSlot));
    }

    @Test
    public void testGetEnergySlot() throws Exception {
        assertThat(timeInfo.getEnergySlot(), is(energySlot));
    }

    @Test
    public void testGetProgramSlot() throws Exception {
        assertThat(timeInfo.getProgramSlot(), is(programSlot));
    }

    @Test
    public void testGetSecondOfDay() throws Exception {
        assertThat(timeInfo.getSecondOfDay(), is(secondOfDay));
    }

    @Test
    public void testGetMinuteOfDay() throws Exception {
        assertThat(timeInfo.getMinuteOfDay(), is(minuteOfDay));
    }

    @Test
    public void testIsFirstOfTracking() throws Exception {
        assertThat(timeInfo.isFirstOfTracking(), is(firstOfTracking));
    }

    @Test
    public void testIsFirstOfEnergy() throws Exception {
        assertThat(timeInfo.isFirstOfEnergy(), is(firstOfEnergy));
    }

    @Test
    public void testIsFirstOfProgram() throws Exception {
        assertThat(timeInfo.isFirstOfProgram(), is(firstOfProgram));
    }
}