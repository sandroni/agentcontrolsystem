package it.rse.retiattive.acs.domain;

import it.rse.util.XmlHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Test of the load file loader.
 *
 * @author Carlo Sandroni
 * @version 1.0, 08/09/2015
 */
public class PlanProgramTest {
  public static final String planFileName = "./input/plan.xml";
  private static Logger logger = Logger.getLogger(PlanProgramTest.class.getName());
  private File file;

  @Before
  public void setUp() throws Exception {
    file = new File(planFileName);
    logger.info("File " + planFileName + " caricato.");
  }

  @Test
  public void testImportXml() throws Exception {
    PlanProgram plans;
    if (file != null) {
      try {
        long startTime = System.nanoTime();
        plans = XmlHelper.fromXml(file, PlanProgram.class);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000;
        System.out.println("Unmashalling took " + duration + "ms");
//        System.out.println(plans.toString());
        PlanProgram.DailyPlan plan = plans.findPlan(new Date());
//        System.out.println(plan.toString());
        for (PlanProgram.Unit unit : plan.getUnits()) {
          for (PlanProgram.Attribute attribute : unit.getAttributes()) {
            PlanProgram.Item item = attribute.findClosestStep(new Date());
            System.out.println("Unit: " + unit.getName() + "; Attribute: " + attribute.getName() + "; Item: " + item);
          }
        }

      } catch (JAXBException e) {
        logger.severe("Formato file non supportato.");
        e.printStackTrace();
      }
    }
  }

  @After
  public void tearDown() throws Exception {


  }
}