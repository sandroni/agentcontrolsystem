package it.rse.retiattive.acs.comm;

import it.rse.retiattive.acs.domain.StateVariable;
import it.rse.util.XmlHelper;
import org.junit.Test;

import javax.xml.bind.JAXBException;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Test DataMessage behaviour and serialization.
 *
 * @author Carlo Sandroni
 * @version 1.0, 28/07/2015
 */
public class DataMessageTest {
  @Test
  public void serialization() throws JAXBException {
    Map<String, Double> vars = new HashMap<>();
    vars.put("Batt1.P", 100.0);
    vars.put("Batt1.SOC", 70.0);

    DataMessage dm = new DataMessage(DataMessage.Action.TELL);
    for (Map.Entry<String, Double> var : vars.entrySet()) {
      dm.addVariable(new StateVariable(var.getKey()).setValue(var.getValue()));
    }
    String xmlData = dm.toXml();
    System.out.println(xmlData);

    DataMessage dmOut = XmlHelper.fromXml(xmlData, DataMessage.class);
    for (StateVariable sv : dmOut.getVarList()) {
      assertThat(vars.get(sv.getName()), is(sv.getValue()));
    }

    for (Map.Entry<String, Double> var : vars.entrySet()) {
      assertThat(dmOut.getVariable(var.getKey()).getValue(), is(var.getValue()));
    }
  }
}